# Using WSL1 to run GUI Linux Applications on Windows 10

I have had great success using WSL1 with Windows 10 to run my standard Ubuntu 20.04 (previously Ubuntu 16.04) setup.  I have set up X Forwarding to a Cygwin X Server from Ubuntu that allows me to run Linux programs, even IntelliJ and java-based simulators, in windows with only a few minor issues.

WSL2 has much better performance for drive access and GPU access, but the GUI setup is very clunky in Windows 10. After the release of WSL2 for Windows 10, I tried to upgrade but ran into major troubles with networking causing running X Windows to disappear whenever I changed network IP address due to VPN.  So for Windows 10, I recommend WSL1 if you want a GUI.  If you just want docker or GPU access, WSL2 should work

These instructions are a streamlined version of the WSL docs: https://docs.microsoft.com/en-us/windows/wsl/  I recommend reviewing those because WSL is constantly improving.

To install wsl 1, open a windows command prompt (click the windows start button and type `cmd`) you will need to run the following commands:
```
wsl --install  # only needed if you have never installed wsl
wsl --set-default-version 1
wsl -l -o  
# the above command shows all available linux instances; verify that Ubuntu-20.04 is listed
wsl --install -d Ubuntu-20.04
```
The above commands should create an Ubuntu 20.04 icon in your start menu.
I recommend pinning the app icon to your task bar so you don't have to open it separately.  I also recommend setting up Windows Terminal, as described in my basic windows setup: [../install.md](install_windows.md)

*******************************************************************
# Set up the default linux environment

Over the years, I've collected a pretty unique shell environment that works well for my development needs.  Some of the files I reference below come from that environment and I put them in a particular spot on my system.  If you plan to copy my setup verbatim,  you may want to clone it before you get started.

```
cd ~
mkdir git-workspace
cd git-workspace
git clone https://bitbucket.org/makro-nrl/makro-toolbox.git makro-toolbox-git
```

If you want to do the same setup, visit [../../install.md](install_linux.md) and run through those instructions before you continue here.

*******************************************************************
# Running with Cygwin/X as the X Server on WSL1

**This option only works with WSL1.**  
See details in the section below on WSL2 if you desire more info.

- Download the latest Cygwin/X Setup file from https://x.cygwin.com/
  - usually, this will be http://cygwin.com/setup-x86_64.exe
    but check the page for details
Install Cygwin/X as usual, and include the `xinit` package.  You could install
more if you will be using cygwin for other purposes. 

- Run the setup, answer the questions using defaults unless you know
  you need other settings.  I usually use the Virginia Tech mirror
  since I'm on the US east coast.

- During install you only need to the `xinit` package unless you
  know you will need additional programs.

- To use cygwin as an Xserver for WSL, you need to do start Xwin with 
  some additional command parameters. This is done by editing the shortcut:
  1. Click the XWin Server Icon in the start menu (I recommend pinning it to the start)
  1. Right click the XWin server Icon and select `More Options>Open File Location`
  1. In the explorer window that appears, right-click the XWin Server Icon
     and select `Properties`
  1. Copy-paste the following in the Target box: 
```
C:\cygwin64\bin\run.exe --quote /usr/bin/bash.exe -l -c "cd; exec /usr/bin/startxwin -- -listen tcp -nowgl -dpi 100"
```

The `-dpi` option can be adjust to your screen resolution.  However, I have not
noticed this option to do any good when trying to adjust screen resolution.
See notes below on screen resolution.

Inside the WSL instance, link to the copy-Xauthority file and run this script each 
time you have trouble connecting because of permissions.  

```
cd ~
ln -s git-workspace/makro-toolbox-git/setup/windows/cygwin/copy-Xauthority
```

You will also need to add `export DISPLAY=:0` to your bash prompt before running any X program.
If you are using the setup from Mak's install you can run the following:
```
cd ~
ln -s git-workspace/makro-toolbox-git/setup/windows/cygwin/EXPORT_CYGWIN_DISPLAY
```
or add this to your appropriate shell configuration.

*******************************************************************
## Accessing your WSL files from windows 

From your bash prompt, you can run `explore.exe .` to 
get a Windows explorer window.  It will open an 9P network connection 
to `\\wsl$` in the current directory and, as far as I can tell, is 
safe for editing or creating files that you share between the two OS instances.

***WARNING: directly modifying files in windows that were created inside of linux
 (outside of `\\$wsl`) will cause your files to disappear!***
Technically, it has something to do with confusing meta-attributes between 
the NTFS and extfs filesystems.  Pragmatically, just don't do it.
(Yes, I've had to restore a back from losing files this way!) 

Going the other way, accessing windows files from ubuntu, couldn't be easier.
Simply use `\mnt\c\` inside your linux instance.  Be aware of some gotchas:

1. Performance.  In a nutshell, accessing files through `\mnt\c` is workable
but is the worst of both worlds, especially if you run a build in linux through
`\mnt\c` that generates a bunch of files causing your windows malware/antivirus
program to start checking every new file bit by @#$%^! bit.  Generally, you 
don't want your build directory to be under antivirus checking, even in a 
strictly-windows toolchain.  Sharing a build directory with WSL in this way
makes performance much worse.  (yes, I tried)
1. There's no easy way to deal with CR/LF differences. Blech!
1. The previously stated issue on accessing files outside of `\\$wsl` still applies.

These are similar issues is the same kind of thing you face with a 
shared folder for a Virtual Machine client.
Best to establish separate development workspaces and use git with a 
remote server to move between the two OS instances or only occasionally 
access the other workspace via `\\$wsl` or `\mnt\c`  as needed.
Since I don't bother to install _any_ development toolchain on windows these
days, this setup really hasn't been as much of an issue.
Mostly, I move images from powerpoint presentations into my WSL instance 
or share a linux file via email or file transfer; using `explorer.exe .` to
 access `\\$wsl\` works well enough for these cases.
 
*******************************************************************
## Rebooting

Rebooting daily can helps keep windows/CygX/WSL1 happier.
(And, data-at-rest is a requirement if you transport any DoD equipment.)
This means loosing all my precious windows, which I detest because it 
greatly increases my morning startup cost.
My solution: tmux with the continuum plugin, which loads your last 
shell environment.  I have 15 shell windows start exactly where 
they were the last time I used them with one command. 
For details, checkout my tmux+zsh setup
at https://bitbucket.org/makro-nrl2/makro-toolbox/src/master/setup/install.md




*******************************************************************
## Notes on screen resolution, un/docking, and font size

Generally, screen resolution is fixed once you run the X Server.  
This means if you change screens or docking, your window sizes can get wonky, 
especially if there is a big difference in resolution between the display
and your laptop.
I'm sure there is some permanent way to fix this, but restarting the
app works most of the time.
In the case of two linux applications, specifically emacs and IntelliJ,
I change the font size based on the setup.
When emacs starts, I open my helper file and run the following orgmode fragment.
```
#+BEGIN_SRC emacs-lisp
;; to increase the font size uncomment the following
;; value is 1/10pt so 100 will yield 10pt font
;;(set-face-attribute 'default nil :height 150)  ;;SB2 laptop
(set-face-attribute 'default nil :height 90) ;;home/work desk
#+END_SRC
#+RESULTS:
```

On my laptop, the resolution for IntelliJ is waaaay too small.
Sometimes [This solution](https://stackoverflow.com/a/51603779) works
and can be enabled by soft linking the file dot-Xresources to ~/.Xresources.
Other times, I have to use [this solution](https://superuser.com/a/1097386)
which can be enabled by soft linking the file sb2_display.sh to my home
directory and sourcing it before I run IntelliJ.

Sometimes when I dock or undock windows will not appear on the desktop
even though I can see them running on the taskbar.
To get them back, I use an old Win98+CygwinX trick:
1. Right click on the window that is hidden in the taskbar
1. Select `Move`
1. Hit any of the arrow keys to get the window to snap to the mouse
1. Click anywhere to drop the window
1. Resize as needed

Alternatively, sometimes windows can be "found" by moving them around
the screen using the WindowsKey+Left or WindowsKey+Right once it has the focus.


*******************************************************************
# Running an X Server for WSL2

If you are on **Windows 11**, WSL2 natively supports gui applications with something called WSLg.  So you no longer need to run a separate x server.  For the latest notes, see my instructions in [wsl2/install.md](install_wsl2.md)

If your still rocking Windows 10, the following notes are relevant. 
WSL2 runs a Microsoft compiled kernel under a _separate_ Virtual Machine.
Thus, it creates a separate virtual network interface meaning the X server 
may not allow connections from this "remote" virtual machine.

It is still possible to get X forwarding to work, though it
is much more work, and there's a huge downside: it kills all
X windows when the network address changes such as when a VPN is started or stopped.
These are not usable solutions for my workflow, 
so I now use WSL1 with Ubuntu 16.04 as shown in the WSL1 section
I then installed and use WSL2 with Ubuntu 18.04.
This allows running my existing setup with WSL1 while using WSL2
with docker for windows 10, which integrates very well with WSL2.
At this time, it seems Microsoft expects to continue
updating WSL1.  Maybe the network issues will be resolved
by the time I am forced to upgrade to WSL2.

If you don't mind the network change issue, the XForwarding
can be solved in one of two ways: tell CygwinX the WSL2's 
network address or install VcXsrv and allow arbitrary 
X windows connections.  I prefer the Cygwin/X solution because 
it allows me to control the network connection.

While testing out WSL2, I did not keep detailed notes on how to get WSL2 going
once I determined the solution would not work.
I would happily give credit to, or link to, the person providing more
detailed install notes for getting this going (hint hint :-).

*******************************************************************
## Using Cygwin/X 

1. Install and start the Cygwin/X as above
1. start the wsl instance and type `ipconfig
2. TBD


*******************************************************************
## Install VcXsrv

https://sourceforge.net/projects/vcxsrv/

Be sure to select allow all connections when starting this!
TBD


*******************************************************************
## Notes that may help if you need to work this out after all
https://github.com/microsoft/WSL/issues/4150

https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2
https://medium.com/@japheth.yates/the-complete-wsl2-gui-setup-2582828f4577
