# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent();
$myWindowsPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myWindowsID);

# Get the security principal for the administrator role
$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator;

Write-Host "Checking admin status.";

# Check to see if we are currently running as an administrator
if ($myWindowsPrincipal.IsInRole($adminRole))
{
    Write-Host "We are running as an administrator, so changing the title and background colour to indicate this.";
    Clear-Host;
}
else {
    # We are not running as an administrator, so relaunch as administrator

    $fp = $script:MyInvocation.MyCommand.Path

    # Start the new process
    Start-Process -FilePath "PowerShell" -Verb RunAs -ArgumentList "-NoExit", "-NoProfile", "-ExecutionPolicy ByPass", "-File `"$($fp)`""

    # Exit from the current, unelevated, process
    Exit;
}

Write-Host "Resetting the Interface Metric...";

Get-NetAdapter | Where-Object {$_.InterfaceDescription -Match "Cisco AnyConnect"} | Set-NetIPInterface -InterfaceMetric 4000

# The name of the interface on following line should be 'vEthernet (WSL)' for versions of windows before Dec 2023
Get-NetIPInterface -InterfaceAlias 'vEthernet (WSL (Hyper-V firewall))' | Set-NetIPInterface -InterfaceMetric 1

Write-Host "Done!";

Write-Host "Leaving this window open so you can see any errors.  Type 'exit' to close window.";
Write-Host -NoNewLine "Press any key to exit...";
# $null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown");
Read-Host -Prompt "Press any key to exit";