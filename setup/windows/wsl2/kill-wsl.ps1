# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent();
$myWindowsPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myWindowsID);

# Get the security principal for the administrator role
$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator;

# Check to see if we are currently running as an administrator
if ($myWindowsPrincipal.IsInRole($adminRole))
{
    # We are running as an administrator, so change the title and background colour to indicate this
    Clear-Host;
}
else {
    # We are not running as an administrator, so relaunch as administrator

    $fp = $script:MyInvocation.MyCommand.Path

    # Start the new process
    Start-Process -FilePath "PowerShell" -Verb RunAs -ArgumentList "-NoProfile", "-ExecutionPolicy ByPass", "-File `"$($fp)`""

    # Exit from the current, unelevated, process
    Exit;
}

taskkill /f /im wslservice.exe
taskkill /f /im wslhost.exe
taskkill /f /im wsl.exe
wsl --shutdown
Get-Service LxssManager | Restart-Service
