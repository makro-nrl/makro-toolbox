
# Notes on WSL2 install

- install wsl as per the usual instructions from microsoft: https://docs.microsoft.com/en-us/windows/wsl/
  - I have sometimes had difficulty installing new linux variants from the microsoft store.  One workaround is to use the install switch with a web-download.  For example, to intall Ubuntu 18.04 from windows command prompt, type `wsl --install -d Ubuntu-18.04 --web-download`. This seems to bypass any login issues in the microsoft store.
- if you will be using WSLg, make sure you update WSLg to the latest release
  from https://github.com/microsoft/wslg/releases
  This is because WSLg updates can fix important bugs between major releases.
- if you will use GPU access, follow the instructions for WSL2 gpu access
  
Fix WSL2 internal networking
============================
The WSL2 network stack does not work when you start it.  On my machines, this is a combination of DNS servers not being set up correctly,the firewall blocking ports I want to use, and the VPN networking disrupting the network stack. 

I have not figured out a way to automate all of this, so I just run the correct setup as needed.  Once the DNS nameservers are added to the resolv.conf file and the firewall is set up properly, it really only requires runing the fix for the VPN when I enable that.

While debugging this, the following commands are your friends:

- `ping`, traceroute for the particular OS you are running on, 
- `ipconfig` for windows and `ifconfig` for linux.   
- Another helpful tip is to use `explore.exe .` to open a windows explorer window in the current linux directory.  (The `vpn-on.sh` script does this for you, which is handy!)

DNS issues
---------------

This solution is borrowed from [here](https://askubuntu.com/a/1398053)

1. copy the sample wsl.conf and resolv.conf files from this directory to the `/etc` directory in the WSL2 instance
1. if you will use VPN, update resolv.conf add your work-specific VPN nameservers to the end 
  - this can be found by using windows command prompt (while on the VPN) `ipconfig /all` and 
    look for the nameservers that windows is using for the VPN network adapter 
  - you can search for how to find your VPN nameserver for further details
1. in the wsl2 instance, also lock the resolv.conf file so it won't be deleted by wsl on reboot
  `sudo chattr +i /etc/resolv.conf`
  You will need to undo this attribute if to edit /etc/resolv.conf
1. double check that this works by running `ping google.com`
1. use `wsl.exe --shutdown` in a windows prompt to stop wsl
1.  start wsl again and try the ping
 
 
Firewall issues
-------------
- Open the following ports in the firewall: 
   TCP: 22(ssh), 80 (http), 443 (https)
   UDP: 23(dns)
   Any other ports you know you will 

VPN issues (Cisco Anyconnect)
---------------
When the VPN is enabled, you need to:

- make sure the DNS servers are listed in your resolv.conf
- Run reset-anyconnect-network-metrics.ps1 from windows by right clicking it and selecting run with powershell.  It will automatically restart the shell in an elevated mode if that is needed.