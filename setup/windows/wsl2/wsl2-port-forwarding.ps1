# Adapted from reportwsus1.ps1 and https://github.com/microsoft/WSL/issues/4150


# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent();
$myWindowsPrincipal = New-Object System.Security.Principal.WindowsPrincipal($myWindowsID);

# Get the security principal for the administrator role
$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator;

# Check to see if we are currently running as an administrator
if ($myWindowsPrincipal.IsInRole($adminRole))
{
    # We are running as an administrator, so change the title and background colour to indicate this
    Clear-Host;
}
else {
    # We are not running as an administrator, so relaunch as administrator

    # On my systems, wsl will not run within an admin shell.
    # So we need to collect the wsl2 ip address from the non-admin account to pass in for the admin execution.
    $remoteport = bash.exe -c "ifconfig eth0 | grep 'inet '"
    $found = $remoteport -match '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';

    if( $found ){
      $remoteport = $matches[0];
      echo "The value of the remoteport is $remoteport"
    } else{
      echo "The Script Exited, the ip address of WSL 2 cannot be found";
      exit;
    }

    $fp = $script:MyInvocation.MyCommand.Path

    # Start the new process
    Start-Process -FilePath "PowerShell" -Verb RunAs -ArgumentList "-NoExit","-NoProfile", "-ExecutionPolicy ByPass", "-File `"$($fp)`"", "$remoteport"

    # Exit from the current, unelevated, process
    Exit;
}

write-host "There are a total of $($args.count) arguments"
for ( $i = 0; $i -lt $args.count; $i++ ) {
    write-host "Argument  $i is $($args[$i])"
} 

$remoteport=$args[0]

echo "The value of the remoteport is $remoteport"
$found = $remoteport -match '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';

if( $found ){
  $remoteport = $matches[0];
} else{
  echo "The Script Exited, the ip address of WSL 2 cannot be found";
  exit;
}

#[Ports]

#All the ports you want to forward separated by coma
#$ports=@(80,443,10000,3000,5000);
$ports=@(22);


#[Static ip]
#You can change the addr to your ip config to listen to a specific address
$addr='0.0.0.0';
$ports_a = $ports -join ",";

# My current setup does not use windows firewall, so i manually unblocked port 22 for WSL
#
# #Remove Firewall Exception Rules
# iex "Remove-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' ";

# #adding Exception Rules for inbound and outbound Rules
# iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Outbound -LocalPort $ports_a -Action Allow -Protocol TCP";
# iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Inbound -LocalPort $ports_a -Action Allow -Protocol TCP";

for( $i = 0; $i -lt $ports.length; $i++ ){
  $port = $ports[$i];
  iex "netsh interface portproxy delete v4tov4 listenport=$port listenaddress=$addr";
  iex "netsh interface portproxy add v4tov4 listenport=$port listenaddress=$addr connectport=$port connectaddress=$remoteport";
}



Write-Host -NoNewLine "Press any key to exit...";
$null = $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown");
