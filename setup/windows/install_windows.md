Options for a windows install
=============

## Install WSL
For any version of WSL, I recommend installing the latest WSL Preview from http://aka.ms/wslstorepage.  

- For Windows 10, I recommend my WSL1+Cygwin setup at [cygwin/install.md](install_wsl1.md)
- For Windows 11, I recommend my WSL2 notes at [wsl2/install.md](install_wsl2.md)

### Windows Terminal as the Bash Shell

I recommend using Windows Terminal, which is available from the microsoft store.
The windows documents have some detailed instructions for using this with WSL that I recommend: https://docs.microsoft.com/en-us/windows/wsl/setup/environment#set-up-windows-terminal.
Some folks who prefer other bash shells than the stock windows shell.
I have hammered on this shell pretty hard and it seems to work most of the
time.  If you use powerline you do need to install some fonts for the
shell (see the Windows portion of my [toolbox install](install_linux.md)
for details).

Once in a while, my tmux gets really messed up with multiple panes inside
the same window.  To clean it up, I just detach, start a new bash shell,
and reattach.  Since tmux keeps everything running in the background, restarting
the bash window is pretty effortless.  This takes five seconds to fix
and happens a few times a week, so fixing this is not [worth my time](https://xkcd.com/1205/).

### Handy Windows + WSL tricks
Open explorer with `explore.exe .` from bash

Create a folder on your desktop called `links` to store common shortcuts that you access on the windows and WSL side.  Any time you open a directory for the second time in a week, add a shortcut to that folder to your link.  

- your windows home directory is usually going to be `c:\Users\<username>\`
- 


### Install Powerline Fonts

- Use TortoiseGit to clone the repo https://github.com/powerline/fonts.git into powerline-fonts-git
- Open a powershell in Admin mode
- change to the directory where you cloned the powerline fonts repo
- run the install file for powershell
    - if you get an execution policy challenge
        -  call `Get-ExectutionPolicy -List`
        -  call `Set-ExectutionPolicy Unrestricted` (remember to set it back!)
        - execute the script
        - answer yes to all the program if it asks
        -  call `Set-ExectutionPolicy <restored value>` (remember to set it back!)
- For your bash shell in WSL's properties, select the font `DejaVu sans mono for powerline`


## Install PowerToys
- Download and install from https://github.com/microsoft/PowerToys/releases/

