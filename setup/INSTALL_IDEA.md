# Install IntelliJ IDEA Community Edition

We highly recommend using IntelliJ IDEA for your python and java projects!
IDEA has a python plugin that is nearly identical to PyCharm, but 
IDEA also integrates with Java code.  With the proper plugin, 
both IDEs display Markdown files, like this one, in a nicely formatted
viewer. 

You just need the **community edition** (i.e., the free version) of IDEA.
There are two ways to install it:
1. On ubuntu, consider using `umake ide idea` to install IDEA.  (If you have a
JetBrains license, replace with `umake ide idea-ultimate`.)
   - For details, see the instructions in the section below.
   - Umake can also install eclipse, IDEA, and other IDEs!
2. Download and set up IDEA (https://www.jetbrains.com/idea/download).

You need to ensure that you install IDEA version 2020.1 or later. 


[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
## Install IDEA plugins
A number of plugins will improve your use of IDEA and provide a streamlined
development environment for our team.

Open `File>Settings` and click `Plugins`  then search the marketplace to 
install the following plugins.

If your desired controller requires python, you must install:
- **Python Plugin** by JetBrains (shown in red in the image below).
You may want to go ahead an install this if you expect to do any python 
programming. 

We also strongly recommend installing the following plugins:

- **GitToolBox** by Jamie Zielinski provides convenient features for git repos.
- **MarkDown** by JetBrains allows you to view and edit our documentation.
- **PlantUML Plugin** by Eugene Steinberg allows you to view and edit our UML diagrams.  
- **Key Promoter X** by hailrutan helps you learn or modify keyboard shortcuts.
- A decent editor keymap. Folks from the team recommend: 
  - **Emacs+ Patched** if you are an emacs user. 
  - **IdeaVIM** if you are a VI user. 
- [add your recommendation here!]

A sample of several plugins is shown below:
![Recommended Plugins](_images/IDEA_RecommendedPlugins.png)


[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
## Set up the rest of the stuff
You should be able to complete the setup information in your relevant connector.



[comment]: ====================================================================
[comment]: ====================================================================
[comment]: ====================================================================
***********************************************************************
### Installing on Ubuntu using Umake 

The following instructions make it possible to install IntelliJ IDEA on Ubuntu.

If on Ubuntu 20.04 or greater:
```
sudo apt install ubuntu-make
```

To install umake version 18.05:
```
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
sudo apt-get update
sudo apt-get install ubuntu-make 
```

Check to make sure ubuntu-make is at least **version 18.05** using 
```umake -version```.

## Installing IntelliJ Community Edition
- `umake ide idea` 
- The default installation path is fine. If you change the installation 
  directory it must be a ***different** folder than the git repository for actorsim*.

### (Optional) Set up a soft link to run idea.sh
If you installed using `umake`, a soft link will make it easier for you
to start idea in the future. 
```
cd ~
ln -s .local/share/umake/ide/idea/bin/idea.sh
```

So you can then just run `~/idea.sh` to start IDEA.


