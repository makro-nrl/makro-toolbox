
# prints machine specs to console.  

echo " "
echo "=============================================================================="
echo "=============================================================================="
echo "## Machine Name"
echo $hostname

echo " "
echo " "
echo "=============================================================================="
echo "=============================================================================="
echo "## Motherboard"
sudo dmidecode -t 2

echo " "
echo " "
echo "=============================================================================="
echo "=============================================================================="
echo "## Memory"
sudo lshw -c memory

echo " "
echo " "
echo "=============================================================================="
echo "=============================================================================="
echo "## Drives and partitions"
sudo lshw -c storage -c disk
