My setup has grown complex enough that I have split it into separate files: <br>
(if you have trouble opening these links on your browser or platform, visit the directory and open them manually because markdown links are interpreted differently on various platforms)

- [[install_linux]] and the associated [[cheatsheet]] <br>
- [[install_windows]]  <br>
- [[install_idea]] <br>
- [[install_obsidian]] <br>

I especially welcome notes and emails to update or fix these!
