
# Wish list

A list of features I eventually want to incorporate into my toolbox:

## Emacs related

It has been a while since I tried to revamp my emacs toolbox.
And I know I would be happier with a few changes in this department!

### Emacs Client
I've always thought it woudl be cool to have a single emacs window.
https://www.gnu.org/software/emacs/manual/html_node/emacs/emacsclient-Options.html

### Doom emacs?
It has so much support and looks much more stable and active than spacemacs
https://github.com/hlissner/doom-emacs

https://www.ethanaa.com/blog/switching-to-doom-emacs/#doom-emacs


### Markdown in emacs
Seems like I'm spending more time in emacs editing markdown...

https://github.com/jrblevin/markdown-mode
https://jblevins.org/projects/markdown-mode/

## zsh related

Check out antigen among this list of essential zsh plugins: https://laymanclass.com/zsh-essential-plugins/