# to enable startup profiling, uncomment the following line AND the last line of this file
# zmodload zsh/zprof

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

#zplug related
# I no longer need zplug because oh-my-zsh now supports docker and docker-compose
#alias zp="zplug"
# source $HOME/.zplug/init.zsh
# zplug "akarzim/zsh-docker-aliases"
# zplug "jhawthorn/fzy", \
#     as:command, \
#     rename-to:fzy, \
#     hook-build:"make && sudo make install"
# zplug 'b4b4r07/enhancd', use:init.sh
# zplug 'b4b4r07/zplug-doctor', lazy:yes
# zplug 'b4b4r07/zplug-cd', lazy:yes
# zplug 'b4b4r07/zplug-rm', lazy:yes
# zplug "b4b4r07/ultimate", as:theme
# zplug "denysdovhan/spaceship-prompt", use:spaceship.zsh, from:github, as:theme
# if ! zplug check --verbose; then
#      printf "Install? [y/N]: "
#     if read -q; then
#         echo; zplug install
#     fi
#     echo
# fi
# zplug load


# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="maks"

## Spaceship font is my favorite, but it runs very slowly on WSL1
# ZSH_THEME="spaceship" 
# SPACESHIP_TIME_SHOW=true
# SPACESHIP_TIME_COLOR=240
# SPACESHIP_USER_COLOR=240
# SPACESHIP_HOST_COLOR_SSH=124
# SPACESHIP_EXEC_TIME_COLOR=240
# SPACESHIP_DIR_COLOR=4
# SPACESHIP_GIT_BRANCH_COLOR=94
# SPACESHIP_PROMPT_ORDER=(
#   # Just comment a section if you want to disable it
#   # time        # Time stamps section (Disabled)
#   # user          # Username section
#   dir           # Current directory section
#   # host          # Hostname section
#   git           # Git section (git_branch + git_status)
#   # hg          # Mercurial section (hg_branch  + hg_status)
#   # package     # Package version (Disabled)
#   # node          # Node.js section
#   # ruby          # Ruby section
#   # elixir        # Elixir section
#   # xcode       # Xcode section (Disabled)
#   # swift         # Swift section
#   # golang        # Go section
#   # php           # PHP section
#   # rust          # Rust section
#   # haskell       # Haskell Stack section
#   # julia       # Julia section (Disabled)
#   # docker      # Docker section (Disabled)
#   # aws           # Amazon Web Services section
#   # gcloud        # Google Cloud Platform section
#   venv          # virtualenv section
#   # conda         # conda virtualenv section
#   # pyenv         # Pyenv section
#   # dotnet        # .NET section
#   # ember       # Ember.js section (Disabled)
#   # kubectl       # Kubectl context section
#   # terraform     # Terraform workspace section
#   # exec_time     # Execution time
#   line_sep      # Line break
#   # battery       # Battery level and status
#   # vi_mode     # Vi-mode indicator (Disabled)
#   jobs          # Background jobs indicator
#   exit_code     # Exit code section
#   char          # Prompt character
# )

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    # command line goodies
    aliases  # 411 for aliases
    common-aliases
    tmux
    fzf
    zsh-interactive-cd
    zsh-navigation-tools

    emacs

    git
    gitfast


    docker  
    docker-compose
    # mvn

    # python related
    python
    pylint
    pep8
    pip
    virtualenv
    virtualenvwrapper  # desired variables are set up in mak-custom.zsh
    
    # web-search
    debian  # includes aliases for apt
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


#help emacs -nw look nicer
TERM=xterm-256color


#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# to enable startup profiling, uncomment the following line AND the first line of this file
# zprof
