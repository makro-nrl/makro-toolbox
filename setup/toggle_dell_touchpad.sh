#!/bin/bash

#accessed 2017-07-12 from https://askubuntu.com/questions/751413/how-to-disable-enable-toggle-touchpad-in-a-dell-laptop

ids=`xinput | grep ALPS |grep Stick | cut -d= -f2 | cut -f1`
echo $ids

for id in $ids
do
    name=`xinput list-props $id | grep ALPS | cut -d\' -f2`
    if xinput list-props $id | grep "Device Enabled (.*):.*1" >/dev/null
    then
	xinput disable $id
	notify-send -u low -i mouse "$name disabled"
    else
	xinput enable $id
	notify-send -u low -i mouse "$name enabled"
    fi
done
