In the descriptions below, modifiers include `Meta`  for `Alt` on a windows or linux machine and `Command` on a Mac,  `Ctrl` for Control.  If I say `<modifier>-<key>` it means press the modifier and the `<key>` at the same time.   For example, `Meta-P` means press `Alt` and `P` together on Windows or linux and  `Command` and `P` on Mac, while `Ctrl-O` means press `Ctrl` and `O`.

## Zsh commands

### Aliases

I use aliases all the time because I spend most of my time managing files in the command console.  However, I haven't had to set up most of the aliases I use because oh-my-zsh installs a set of pretty handy ones.

One of the problems with so many aliases is that I forget what they are if I don't use them often. 
 So I have an alias to search the list of aliases: `aG content`  (short for `alias grep` or `alias G`, where `G` is a zsh alias for grep) allows you to search for all aliases that contain `content`.  For example, try the command `aG git` to see all the useful aliases you can use for git.

`ltr` and its related aliases `ltrh` and `ltra` are useful aliases for viewing the contents of a large directory in long mode.  Use `aG ltr` to see what these aliases do, and read through the manual for ls (`man ls`) to understand what the various options do.  

If you installed `exa` and want to see a visual, text-based version of the directory struction, the `extr` aliases are pretty handy .  Try `extr2` or `extr3`.  Have a look at the possible aliases for exa using `aG exa`.  


### Movement 
`Meta P` and `Meta N` - _search_ backward through in history; the history is pretty great because you can filter the search by starting to type the command you're interested in.  Suppose you are trying to run a script `python scriptname` and doing a bunch of stuff in between running the script.   When you want to run the script again, you can just type `python` and then `Meta P`.

`Ctrl-F` and `Ctrl-B` to move cursor forward or back on current line
`Left` or `right` arrows move the cursor forward or back
`Ctrl-Left` or `Ctrl-Right` jump the cursor forward or back by word

#TODO Notes to integrate 
use ctrl +n +p +b +f (next previous line and backwards or forwards a character)
use ctrl +a +e to go to start or end of line

## Git helpers

, you can run the `glods` alias (git log datestamp) and it will show you where your current branch remotes are wrt your commits.  

Also, the gst (git status) command will tell you that you are ahead or behind in your commit history as an indicator.

## Tmux 
If you interact with your command line prompt (or linux shell) more than a few times a day, then tmux is the biggest time saver you will ever use.  My setup allows you to keep a standard set of directories open regardless of whether you reboot your computer.  In other words, for each of you common directories, you can open a shell one time and forever have it at your finger tips.  However, you will need to leverage my setup if you want to gain this functionality because I install a couple of programs that unlock this functionality.

Have a look at tmux can do at https://github.com/tmux/tmux.  

All tmux commands in this setup begin with the character `Ctrl-Q`.  From now on, I will list the leader key as `T<key>` when I mention a command, where you would press `Ctrl-Q <lift> <key>`. 

Tmux is actually a bunch of "scripts" that you can run from any terminal.  The commands are listed by opeing the help `?` screen.  But most of the common commands are also bound to hot keys that you can run within tmux.

- `T?` help  (`esc` to leave)

### window commands
- `Tc` Create Window
- `T,` Rename Window
- `TN` for `N=[0-9]` to move to windows 0 thru 9
- `Tw` for choosing a window, then selec the number or `Meta` combination in the first column

### Continuum and hibernate
As I mentioned, I have added a couple of plugins that save the current setup of your tmux session and reload it when you restart tmux after a reboot.  The session is saved every 15 minutes, but you can force a save using `T Ctrl-s`.  You should see a note flash in the tmux status are that the the Tmux environment was saved.  If it wasn't, you might need to run `TI` to update your tmux session.  If that doesn't reload tmux and install some programs, double check that you have set up your environment according to my instructions in [[install]].

### Copy mode
Tmux copy mode is very powerful because it allows you to copy text from the console to the system clipboard as well as copy text between terminals -- all without using your mouse.

As an example, suppose you want to copy files from some distant directory to your current position in window 1 (accessed with `T1`.  Easy to do!  Just do the following:

- start a new shell with `Tc`, 
- (optionally, you can rename this with `T,`)
- use `cd` to move to the directory you want to copy from
- type `pwd` to list out the path of that directory
- start copy mode with `T[`  (abort with `q`)
- use motion keys to get to the start or end of the directory name
- hit `Ctrl-Space` to start the copy buffer
- use motion keys to move the the opposite end of the directory name
- you should see a highlighed buffer over the text you are about to yank
- use `y` to yank into the buffer.  
- move back to the first window with `T1` 
- use `T]` to paste the directory

As an added bonus, if you have installed my setup, then this also has been placed on the system buffer.  That means you can copy a bunch of text from your terminal into the system buffer to send an email or capture an error, etc.

Furthermore, if you consistently use this setup, it's easy to cut and paste between machines.  Suppose you are on your developer machine and see an error you want to post on a chat or email to someone.  But suppose you must send messages and email from another machine. 
 No big deal!  Just shell into your develper machine from your email machine, start tmux (which will steal the session from your developer machine), use copy mode, and paste it in your email.  

## Windows Terminal goodies

If you're on WSL, I recommend windows terminal, as mentioned in my 


CTRL-TAB CTRL-SHIFT-TAB to move left and right in the tabs
