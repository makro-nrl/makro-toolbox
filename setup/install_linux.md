# Baseline shell install 

These are my installation instructions for Ubuntu 20.04 Linux machine.
Previously, this worked on an Ubuntu 16.04 machine.

Many steps of this installation require checking for recent versions
so I don't bother to make this into a script.
It only takes about 30 minutes to get this setup installed.

Update apt and ubuntu, then install the bare minimum to "git" your toolbox
=======================================
```
sudo apt update
sudo apt upgrade
sudo apt install zsh emacs git ssh
```


Add your ssh key to BitBucket, if needed
=========================================

First, determine if you have a public key:
```
ls -la ~/.ssh/
```
If you don't see a file called `id_rsa.pub` then you need to run `ssh-keygen`
and follow the prompts, but do **not** add a password or you will
need to type a password everytime git interacts with a remote server.

Now you need to copy this key to your bitbucket account.
First, get a copy of the key:
```
cat ~/.ssh/id_rsa.pub
```
and copy everything between the command prompts.

Open your bitbucket account, go to profile settings, find the SSH keys
area, and add the key with a name you can remember.

Clone this setup repository
===========================
```
cd ~
mkdir git-workspace
cd git-workspace
git clone https://bitbucket.org/makro-nrl/makro-toolbox.git makro-toolbox-git
```

Note the suffix of `-git` to help me distinguish local git repositories.
I don't use the extension `.git` because that is conventionally reserved
for remote, bare repositories.



Install base programs
=======================================
```
sudo apt install zsh emacs python3 python3-pip curl git zip unzip virtualenvwrapper silversearcher-ag fd-find tmux pandoc  
sudo pip3 install --upgrade pip
```
This installs a number of very useful tools that I recommend.   If you haven't seen these before, I recommend reading these websites while you wait for the install to complete:

- virtualenv, an lightweight way to install python dependencies: https://virtualenv.pypa.io/en/latest/  (Conda is also great!)  
- fd, a faster replacement for find: https://github.com/sharkdp/fd/
- ag, or silversearcher, a better replacement for grep+awk: https://github.com/ggreer/the_silver_searcher
- exa, a superb replacement for ls: https://the.exa.website/
- tmux, a terminal multiplexer: https://github.com/tmux/tmux
- pandoc, the swiss army knife of converting documents from one format to another: https://pandoc.org


Install Exa by hand
-------------------------------------------------

For Ubuntu 20.10 and later:
```
sudo apt intall exa
```

Before Ubuntu 20.10:

- install rust (details here )
```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

- grab the latest exa release and unzip from https://github.com/ogham/exa/releases
```shell
cd ~
mkdir exa
cd exa
wget https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip
unzip exa-linux-x86_64-0.9.0.zip
sudo cp exa-linux-x86_64 /usr/local/bin/exa
```


Switch to zsh as the default 
=============================
First, determine where zsh is located using `which`. Then update your shell with `chsh`.:

```
which zsh
```

You should see a set of responses similar to the following 
```
chsh
```
chsh


Install oh-my-zsh happiness
===========================

- install oh-my-zsh:
```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

- install zplug:
```
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
```


Link your custom configs
=========================
```
cd ~
mv .zshrc .zshrc.bak
ln -s git-workspace/makro-toolbox-git/setup/dot-zshrc .zshrc
ln -s git-workspace/makro-toolbox-git/setup/dot-tmux.conf .tmux.conf
cd ~/.oh-my-zsh/custom
ln -s ~/git-workspace/makro-toolbox-git/setup/mak-custom.zsh
cd ~/.oh-my-zsh/themes
ln -s ~/git-workspace/makro-toolbox-git/setup/maks.zsh-theme
```

(**OPTIONAL!**) My favorite zsh theme is spaceship, but I found it is too slow on WSL.
If you want to try it out, run these lines and change the .zshrc to point to this theme instead. 
```
cd ~/.oh-my-zsh/custom/themes
git clone https://github.com/denysdovhan/spaceship-prompt.git spaceship-prompt-git
ln -s spaceship-prompt-git/spaceship.zsh-theme
```

Install tmux plugins
====================
- start tmux
- run <command>-I (Ctrl-Q I) to install tmux plugins


Install SDKMAN and Java
=======================
```
curl -s "https://get.sdkman.io" | bash
sdk list java 
# choose a good version to install from Java.net
# Generally, the latest Java 11 is fine for now
# Replace the version below with the one you want to install
sdk install java 11.0.12-open
```

Install Umake and IDEs of interest
==================================
Ubuntu-make (umake) can install various IDEs such as IntelliJ, eclipse, IDEA, vscode, and others!


If on Ubuntu 20.04 or greater:
```
sudo apt install ubuntu-make
```

Otherwise, 
```
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make 
sudo apt update
sudo apt install ubuntu-make
```

## Install IntelliJ
Open [INSTALL_IDEA.md](makro-toolbox-git/setup/install_idea.md) and follow the instructions.


Install Latex 
==================================

```
sudo apt install texlive-base texlive-latex-recommended texlive-science texlive-latex-extra texlive-pstricks evince
```

