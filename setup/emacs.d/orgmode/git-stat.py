#! /usr/bin/python

from os import listdir
from os.path import isfile, join, isdir
import git

base_path = "."
print "Working in '{}'".format(base_path)

def parse_dirs(path):
    dirs = [f for f in listdir(path) if isdir(join(path, f))]
    return dirs

def repo_stat(repo_path):
    #print("repo path:{0}".format(repo_path))
    needs_fix = False
    output = []
    # output.append("")
    # output.append("")
    # output.append("")
    # output.append("")
    # output.append("------------------------------------------------------")
    output.append("------------------------------------------------------")
    output.append("------ checking repo path {0}".format(repo_path))
    repo = git.Repo(repo_path)
    stat = repo.git.status("-bs")

    if len(repo.remotes) <= 0:
        output.append("!!!!! WARNING: Repository lacking any remote references !!!!!")
        needs_fix = True

    for remote in repo.remotes:
        url = remote.url
        output.append("   {} {}".format(remote, url))

#    stat = repo.git.status()
    #print "'{}'".format(stat)
    lines = stat.split('\n');
    for line in lines:
        #print 'line:{}'.format(line)
        if line.find("ahead") > 0:
            output.append("   {}".format(line))
            needs_fix = True
        if line.startswith(" M") > 0:
            output.append("   {}".format(line))
            needs_fix = True
        if line.startswith("??") > 0:
            output.append("   {}".format(line))
            needs_fix = True
    return output, needs_fix

base_dirs = parse_dirs(base_path)
verbose = False

for dir in base_dirs:
    if dir.find("-git") >= 0:
        if verbose:
            print("")
            print("")
            print("=====================================================================")
            print("====== examining directory {}".format(dir))
            print("=====================================================================")
        else:
            print("====== checking dir {}".format(dir))
        sub_dir = join(base_path, dir)
        output, needs_fix = repo_stat(sub_dir)
        if needs_fix:
            for line in output:
                print line

for dir in base_dirs:
    if dir.find("-git") < 0:
        sub_dir = join(base_path, dir)
        sub_sub_dirs = parse_dirs(sub_dir)
        if verbose:
            print("=====================================================================")
            print("====== examining subdirectory {}".format(sub_dir))
            print("=====================================================================")
        else:
            print("====== checking dir {}".format(dir))
        for sub_sub_dir in sub_sub_dirs:
            #print("subdir:{} git found at {}".format(sub_sub_dir, sub_sub_dir.find("-git")))
            if sub_sub_dir.find("-git") >= 0:
                #print("git dir: {}".format(sub_sub_dir))
                repo_dir = join(sub_dir, sub_sub_dir)
                output, needs_fix = repo_stat(repo_dir)
                if needs_fix:
                    for line in output:
                        print line
