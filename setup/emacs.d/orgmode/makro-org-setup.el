;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; general emacs setup that you like
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

					;(setq line-move-visual t) 
;;(setq global-visual-line-mode t) ;;wrap long lines on the word boundary
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
(package-initialize)

;;Much of this setup was derived from Brent Hansen's setup at http://doc.norang.ca/org-mode.html

(require 'org)

;;emacs: stfu!
(setq visible-bell 1)

;;Some formatting that I like 
;;  from http://stackoverflow.com/questions/1771981/how-to-keep-indentation-with-emacs-org-mode-visual-line-mode
(add-hook 'org-mode-hook (lambda () (visual-line-mode 1)))
(add-hook 'org-mode-hook (lambda () (org-indent-mode t)))
;(add-hook 'org-mode-hook (lambda () (auto-fill-mode t))) ;; split lines at right margin as you type

;;enable flyspell
;;ispell setup -- on windows this requires installing aspell in cygwin
;; note: I tried hunspell but could not get hunspell to find the dictionaries
;(custom-set-variables
; '(ispell-program-name "C:/cygwin64/bin/aspell.exe"))
(add-hook 'org-mode-hook (lambda () (flyspell-mode t)))

;;enable flyspell for text mode and tex files
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'TeX-mode-hook 'flyspell-mode)


;;;;;;;;;;;;;;;;;;;
;;Enable langtool 
;;  from http://emacs.stackexchange.com/questions/2171/what-options-are-there-for-writing-better-non-programming-text-in-emacs
;;  note: requires installing the langtool.el package from your toolbox
;; (add-hook 'org-mode-hook (lambda ()
;;    (progn
;;      ;; Grammar
;;      (require 'langtool)
;;      (setq langtool-language-tool-jar "/path/to/LanguageTool.jar"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;setup files/directories for this machine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;note: I'm deliberately setting globally accessible variables in a local scope so 
;;      a clear error will be produced for unknown machines or borked directories.
(print (system-name))

(if (gnus-string-equal (system-name) "PRELUDE")  ;; my personal laptop
  (progn 
    (print "Loading setup for personal windows laptop")
    (setq makro-dropbox-dir "Z:/files/cloudstore/Dropbox/")
    (setq makro-dropbox-shared-all-org-dir (concat makro-dropbox-dir "shared/all-devices/"))
    (setq makro-dropbox-shared-nrl-org-dir (concat makro-dropbox-dir "shared/nrc/"))
    (setq makro-dropbox-shared-research-org-dir (concat makro-dropbox-dir "shared/research/"))

    (setq makro-personal-workspace "Z:/files/workspace/")
    (setq makro-personal-org-dir (concat makro-personal-workspace "org-personal.git/"))
    (setq makro-dropbox-personal-org-dir (concat makro-dropbox-dir "personal/flow/org/"))
    (setq org-directory makro-personal-org-dir)
    (setq org-agenda-files
      (list
         makro-dropbox-personal-org-dir ;;for mobileorg and web access as needed
	 makro-dropbox-shared-all-org-dir ;;shared with other users/devices
	 makro-dropbox-shared-nrl-org-dir ;;shared with nrl only
	 ))

    (setq org-agenda-files
	  (append
	   (org-agenda-files)
	   (file-expand-wildcards (concat makro-personal-workspace "*/*.org")) ;;new projects in workspace that have org files in them
	   ))

    (setq makro-diary-file  (concat makro-personal-org-dir "personal-diary.org"))
    (setq makro-refile-file (concat makro-personal-org-dir "personal-refile.org"))
    (setq makro-shared-all-refile-file (concat makro-dropbox-shared-all-org-dir "shared-all-refile.org"))
    (setq makro-shared-nrl-refile-file (concat makro-dropbox-shared-nrl-org-dir "shared-nrl-refile.org"))
    (setq makro-shared-research-refile-file (concat makro-dropbox-shared-research-org-dir "shared-research-refile.org"))

 ))

(if (gnus-string-equal (system-name) "WN14GFE20")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for work laptop")
    (setq makro-dropbox-dir "c:/z/cloud/Dropbox/")
    (setq makro-dropbox-shared-all-org-dir (concat makro-dropbox-dir "shared/all-devices/"))
    (setq makro-dropbox-shared-nrl-org-dir (concat makro-dropbox-dir "shared/nrc/"))
    (setq makro-dropbox-shared-research-org-dir (concat makro-dropbox-dir "shared/research/"))

    (setq makro-nrl-org-dir "C:/s/workspace/org-nrl.git/")
    ;;no personal directory  on this machine
    (setq org-directory makro-nrl-org-dir)
    (setq org-agenda-files
      (list
         ;;no personal files on this machine 
         ;;makro-nrl-org-dir  ;;now included via wildcard below
	 makro-dropbox-shared-all-org-dir ;;shared with other users/devices
	 makro-dropbox-shared-nrl-org-dir ;;shared with nrl only
	 makro-dropbox-shared-research-org-dir ;;my common research files
	 ))

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
	  (append
	   (org-agenda-files)
	   (file-expand-wildcards "C:/s/workspace/actorsim.git/doc/*.org")
	   (file-expand-wildcards "C:/s/workspace/*/*.org")      ;;any subdirectory of workspace
	   (file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*.org")) ;;within yaer
	   (file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*/*.org")) ;;within year/subproject
	   ))
    

    (setq makro-diary-file  (concat makro-nrl-org-dir "nrl-diary.org"))
    (setq makro-refile-file (concat makro-nrl-org-dir "nrl-refile.org"))
    (setq makro-shared-all-refile-file (concat makro-dropbox-shared-all-org-dir "shared-all-refile.org"))
    (setq makro-shared-nrl-refile-file (concat makro-dropbox-shared-nrl-org-dir "shared-nrl-refile.org"))
    (setq makro-shared-research-refile-file (concat makro-dropbox-shared-research-org-dir "shared-research-refile.org"))

 ))

(if (gnus-string-equal (system-name) "LN14GFE40")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for linux work laptop")
    ;(setq makro-dropbox-dir "c:/z/cloud/Dropbox/")
    ;(setq makro-dropbox-shared-all-org-dir (concat makro-dropbox-dir "shared/all-devices/"))
    ;(setq makro-dropbox-shared-nrl-org-dir (concat makro-dropbox-dir "shared/nrc/"))
    ;(setq makro-dropbox-shared-research-org-dir (concat makro-dropbox-dir "shared/research/"))

    (setq makro-nrl-org-dir "~/workspace/org-nrl-git/")
    ;;no personal directory  on this machine
    (setq org-directory makro-nrl-org-dir)
    ;; (setq org-agenda-files
    ;;   (list
    ;;      ;;no personal files on this machine 
    ;;      ;;makro-nrl-org-dir  ;;now included via wildcard below
    ;; 	 ;;makro-dropbox-shared-all-org-dir ;;shared with other users/devices
    ;; 	 ;;makro-dropbox-shared-nrl-org-dir ;;shared with nrl only
    ;; 	 ;;makro-dropbox-shared-research-org-dir ;;my common research files
    ;; 	 ))

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
	  (append
	   (file-expand-wildcards "~/workspace/*/*.org")  ;;any subdir of workspace
	   (file-expand-wildcards "~/workspace/papers/*/*.org") ;;any subdirectory of workspace/papers
	   (file-expand-wildcards "~/workspace/projects/*/*.org") ;;any subdirectory of workspace/projects
	   (file-expand-wildcards "~/workspace/proposals/*/*.org") ;;any subdirectory of workspace/proposals
	   (file-expand-wildcards "~/workspace/workshops/*/*.org") ;;any subdirectory of workspace/workshops
	   (file-expand-wildcards "~/workspace/reading/*/*.org") ;;any subdirectory of workspace/reading
	   (file-expand-wildcards "~/workspace/travel/*/*.org") ;;any subdirectory of workspace/travel
	   (file-expand-wildcards "~/workspace/actorsim/*/*.org") ;;any subdirectory of workspace/actorsim
	   
	   ;; (append
	   ;(org-agenda-files)
	   ;; (file-expand-wildcards "~/workspace/actorsim.git/doc/*.org")
	   ;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*.org")) ;;within yaer
	   ;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*/*.org")) ;;within year/subproject
	  ))
    

    (setq makro-diary-file  (concat makro-nrl-org-dir "nrl-diary.org"))
    (setq makro-refile-file (concat makro-nrl-org-dir "nrl-refile.org"))
    ;(setq makro-shared-all-refile-file (concat makro-dropbox-shared-all-org-dir "shared-all-refile.org"))
    ;(setq makro-shared-nrl-refile-file (concat makro-dropbox-shared-nrl-org-dir "shared-nrl-refile.org"))
    ;(setq makro-shared-research-refile-file (concat makro-dropbox-shared-research-org-dir "shared-research-refile.org"))

 ))

(if (gnus-string-equal (system-name) "ip-172-31-28-154.us-west-2.compute.internal")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for AWS server")
    ;;(setq makro-dropbox-dir "c:/z/cloud/Dropbox/")
    ;;(setq makro-dropbox-shared-all-org-dir (concat makro-dropbox-dir "shared/all-devices/"))
    ;;(setq makro-dropbox-shared-nrl-org-dir (concat makro-dropbox-dir "shared/nrl/"))
    ;;(setq makro-dropbox-shared-research-org-dir (concat makro-dropbox-dir "shared/research/"))

    (setq makro-personal-org-dir "~/workspace/org-personal.git/")
    ;;(setq makro-dropbox-personal-org-dir (concat makro-dropbox-dir "personal/flow/org/"))
    ;;(setq org-directory makro-personal-org-dir)
    (setq org-agenda-files
      (list
         makro-personal-org-dir
         ;;makro-dropbox-personal-org-dir ;;for mobileorg and web access as needed
	 ;;makro-dropbox-shared-all-org-dir ;;shared with other users/devices
	 ;;makro-dropbox-shared-nrl-org-dir ;;shared with nrl only
	 ;;makro-dropbox-shared-research-org-dir ;;my common research files
	 ))

    (setq makro-diary-file  (concat makro-personal-org-dir "personal-diary.org"))
    (setq makro-refile-file (concat makro-personal-org-dir "personal-refile.org"))
    ;;(setq makro-shared-all-refile-file (concat makro-dropbox-shared-all-org-dir "shared-all-refile.org"))
    ;;(setq makro-shared-nrl-refile-file (concat makro-dropbox-shared-nrl-org-dir "shared-nrl-refile.org"))
    ;;(setq makro-shared-research-refile-file (concat makro-dropbox-shared-research-org-dir "shared-research-refile.org"))

 ))

(if (gnus-string-equal (system-name) "u16main")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for personal ubuntu main")
    ;; (setq makro-dropbox-dir "Z:/files/cloudstore/Dropbox/")
    ;; (setq makro-dropbox-shared-all-org-dir (concat makro-dropbox-dir "shared/all-devices/"))
    ;; (setq makro-dropbox-shared-nrl-org-dir (concat makro-dropbox-dir "shared/nrc/"))
    ;; (setq makro-dropbox-shared-research-org-dir (concat makro-dropbox-dir "shared/research/"))

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
	  (append
	   (org-agenda-files)
	   (file-expand-wildcards "~/workspace/*/*.org")      ;;any subdirectory of workspace
	   ;; (file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*.org")) ;;within yaer
	   ;; (file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*/*.org")) ;;within year/subproject
	   ))

    (setq makro-personal-org-dir "~/workspace/org-personal-git")
    (setq makro-diary-file  (concat makro-personal-org-dir "personal-diary.org"))
    (setq makro-refile-file (concat makro-personal-org-dir "personal-refile.org"))
    ))

(setq org-default-notes-file  makro-refile-file)
(setq org-agenda-diary-file makro-diary-file)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     startup and keybinding tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;start all files using pretty indentation, pretty please!
(setq org-startup-indented t)

(load-file "makro-org-keybindings.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           todo tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-todo-keywords
  `((sequence 
     "TODO(t)" 
     "NEXT(n)" 
     "|" 
     "DONE(d)")

    (sequence
     "WAITING(w@/!)" 
     "HOLD(h@/!)" 
     "|" 
     "CANCELLED(c@/!)" 
     "PHONE" 
     "MEETING")))


;;to see a list of colors, type M-x list-colors-display
(setq org-todo-keyword-faces
  (quote (("TODO" :foreground "dark grey" :weight bold)
          ("NEXT" :foreground "dark green" :weight bold)
          ("DONE" :foreground "dark goldenrod" :weight bold)
          ("WAITING" :foreground "orange" :weight bold)
          ("HOLD" :foreground "tomato" :weight bold)
          ("CANCELLED" :foreground "red" :weight bold)
          ("MEETING" :foreground "purple" :weight bold)
          ("PHONE" :foreground "purple" :weight bold))))

(setq org-use-fast-todo-selection t)

(setq org-treat-S-cursor-todo-selection-as-state-change nil)

(setq org-todo-state-tags-triggers
  (quote (("CANCELLED" ("CANCELLED" . t))
          ("WAITING" ("WAITING" . t))
	  ("HOLD" ("WAITING") ("HOLD" . t))
	  (done ("WAITING") ("HOLD"))
	  ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
	  ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
	  ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

;;prevent tasks with unfinished subtasks from transitioning to done
(setq org-enforce-todo-dependencies t)

;; if you find you are un/checking checkboxes on repeating tasks, use the following
;; (add-to-list 'load-path (expand-file-name "~/git/org-mode/contrib/lisp"))
;; (require 'org-checklist)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           capture tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-capture-templates
  '(
    ("h" "Habit" entry (file makro-refile-file)
     "* NEXT %?\nCreated:%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n")
    
    ("j" "Journal" entry (file+datetree makro-diary-file) 
     "* %?\nCreated:%U\n" :clock-in nil :clock-resume nil)
    
    ("m" "Meeting" entry (file makro-refile-file)
     "* MEETING with %? :MEETING:\nCreated:%U" :clock-in nil :clock-resume nil)
    
    ("n" "note" entry (file makro-refile-file)
     "* %? :NOTE:\nCreated:%U\n%a\n" :clock-in nil :clock-resume nil)
    
    ("p" "Phone call" entry (file makro-refile-file) "* PHONE %? :PHONE:\nCreated:%U" :clock-in nil :clock-resume nil)
    
    ("t" "todo" entry (file makro-refile-file)
     "* TODO %?\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
    
    ("s" "Saved entries to dropbox")
      ("sa" "shared/all-devices")
        ("sat" "shared all todo" entry (file makro-shared-all-refile-file)
	 "* TODO %?\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
    	("san" "shared all note" entry (file makro-shared-all-refile-file)
	 "* %? :NOTE:\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
	("sap" "shared all Phone call" entry (file makro-shared-all-refile-file)
	 "* PHONE %? :PHONE:\nCreated:%U" :clock-in nil :clock-resume nil)
    
      ("sn" "shared/nrl")
        ("snt" "shared nrl todo" entry (file makro-shared-nrl-refile-file)
	 "* TODO %?\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
	("snn" "shared nrl note" entry (file makro-shared-nrl-refile-file)
	 "* %? :NOTE:\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
    
      ("sr" "shared/research")
        ("srt" "shared nrl todo" entry (file makro-shared-research-refile-file)
	 "* TODO %?\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
	("srn" "shared nrl note" entry (file makro-shared-research-refile-file)
	 "* %? :NOTE:\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
	("srr" "shared nrl reference" entry (file makro-shared-research-refile-file)
	 "* %? :REFERENCE:\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
    
    ;;requires linking to thunderlink & gmail
    ;;("r" "respond" entry (file makro-refile-file)
    ;;"* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" 
    ;; :clock-in nil :clock-resume nil :immediate-finish t)
    
    ;;requires linking via org-protocol
    ;;("w" "org-protocol" entry (file makro-refile-file)
    ;;"* TODO Review %c\n%U\n" :immediate-finish t)
    ))

;; If you capture an item in less than a minute, it creates an empty LOGBOOK drawer
;; so remove empty LOGBOOK drawers on clock out
(defun bh/remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line 0)
    (org-remove-empty-drawer-at "LOGBOOK" (point))))

(add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           refile tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-log-refile t)
(load-file "bh-refile-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           agenda tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-agenda-start-day "10d")
;(setq org-agenda-span 'day) ;;use if the agenda is loading very slowly

(load-file "makro-agenda-tweaks.el")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           link tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-confirm-shell-link-function 'y-or-n-p)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           tag tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Tags with fast selection keys
(setq org-tag-alist '(("GO" . ?g)
		      ("FOCUS" . ?f)            ;;focus on this project first when planning a day
		      ("PROJECT" . ?p)          ;;a long-running task for tracking subtasks and appointments
		      ("SPRINT_NOTE" . ?n)      ;;a note to show on all sprint agendas
		      ("SPRINT_TARGET" . ?t)    ;;subtasks to target during this sprint
		      ("THIS_SPRINT" . ?1)        ;;projects & tasks for this sprint
		      ("NEXT_SPRINT" . ?2)      ;;projects & tasks for the next sprint
		      ("SPRINT_BACKLOG" . ?3)   ;;backlogged projects & tasks
		      ("QUAD_CURRENT" . ?4)
		      ("QUAD_NEXT" . ?5)
		      ("QUAD_BACKLOG" . ?6)
		      ("YEAR_2017" . ?7) ;; 1 year goals
		      ("YEAR_2019" . ?8) ;; 2 year goals
		      ("YEAR_2021" . ?9) ;; 5 year goals
		      ("YEAR_2026" . ?0) ;;10 year goals
		      ("YEAR_2036" . ?-) ;;20 year goals
		      ("VISION" . ?v)    ;;mission statements
		      ("YAR" . ?y)       ;;things you should probably report on your YAR
		      ("BOOKMARK" . ?b)         ;;quick access to links and notes
		      ("PROJECT_IDEAS" . ?q)    ;;project ideas (someday/maybe)
		      ("ARCHIVE" . ?a) 
		      ("WAITING" . ?w)
		      ("HOLD" . ?h)
		      ("CANCELLED" . ?c)
		      ("FLAGGED" . ??)
		      (:startgroup . nil)
		      ;; ("@online" . ?O)
		      ;; ("@errand" . ?E)
		      ;; ("@nrl" . ?N)
		      (:endgroup . nil)    ))

(setq org-tag-persistent-alist org-tag-alist)

; Allow setting single tags without the menu
;(setq org-fast-tag-selection-single-key (quote expert))

; For tag searches ignore tasks with scheduled and deadline dates
(setq org-agenda-tags-todo-honor-ignore-options t)

;Tag current entry automatically if any words match org-tag-alist or org-tag-persistent-alist
;might be usefule to add this to org-capture-before-finalize-hook
;found here: http://stackoverflow.com/questions/29788639/automatically-assigning-tags-in-org-mode
;; (defun org-auto-tag ()
;;   (interactive)
;;   (let ((alltags (append org-tag-persistent-alist org-tag-alist))
;;         (headline-words (split-string (org-get-heading t t)))
;;         )
;;     (mapcar (lambda (word) (if (assoc word alltags)
;;                              (org-toggle-tag word 'on)))
;;             headline-words))
;;     )



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           reminder tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(load-file "bh-reminder-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           focus tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-file "bh-focus-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           Agenda view tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-file "bh-agenda-view-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Task and Structure Tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-file "bh-structure-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           IDO tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-completion-use-ido t)
(setq ido-everywhere t)
(setq ido-max-directory-size 100000)
(ido-mode (quote both))
; Use the current window when visiting files and buffers with ido
(setq ido-default-file-method 'selected-window)
(setq ido-default-buffer-method 'selected-window)
; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)


(print org-tag-alist)

(print "Success!")
