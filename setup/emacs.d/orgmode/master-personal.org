#+STARTUP: showall indent

Load the default setup by pressing C-c C-c in the following block and answering yes.
#+BEGIN_SRC emacs-lisp
  (load-file "spacemacs-org.el")
#+END_SRC
#+RESULTS:
: t

#+BEGIN_SRC emacs-lisp
  (load-file "makro-org-setup.el")
#+END_SRC
#+RESULTS:
: t

#+BEGIN_SRC emacs-lisp
 (setq org-agenda-files
   (append (org-agenda-files)
            makro-dropbox-shared-research-org-dir ;;my common research files
 ))
#+END_SRC
#+RESULTS:
: (Z:/files/cloudstore/Dropbox/personal/flow/org/dropbox-refile.org Z:/files/cloudstore/Dropbox/shared/all-devices/shared-all-refile.org Z:/files/cloudstore/Dropbox/shared/all-devices/shared-notes.org Z:/files/cloudstore/Dropbox/shared/all-devices/shared-refile.org Z:/files/cloudstore/Dropbox/shared/nrc/shared-nrl-refile.org Z:/files/cloudstore/Dropbox/shared/nrc/shared-refile-nrl.org z:/files/workspace/org-personal.git/annual-plan.org z:/files/workspace/org-personal.git/financial.org z:/files/workspace/org-personal.git/personal-diary.org z:/files/workspace/org-personal.git/personal-overview.org z:/files/workspace/org-personal.git/personal-refile.org z:/files/workspace/org-personal.git/personal-templates.org z:/files/workspace/org-personal.git/personal.org z:/files/workspace/org-personal.git/ssdc.org z:/files/workspace/org-personal.git/template-daily.org z:/files/workspace/org-personal.git/template-sprint.org z:/files/workspace/org-personal.git/template-weekly.org . Z:/files/cloudstore/Dropbox/shared/research/)

[[z:/files/workspace/org-personal.git/personal-overview.org][Personal Overview.org]]
