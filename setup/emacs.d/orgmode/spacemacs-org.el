;; Much of this setup was inspired by from Bernt Hansen's setup at http://doc.norang.ca/org-mode.html
(require 'org)
(setq calfw-dir "~/.emacs.d/emacs-calfw")
(if (file-directory-p calfw-dir)
    (progn
      (add-to-list 'load-path calfw-dir)
      (require 'calfw-org)
      ;; Unicode characters
      (setq cfw:fchar-junction ?╋
	    cfw:fchar-vertical-line ?┃
	    cfw:fchar-horizontal-line ?━
	    cfw:fchar-left-junction ?┣
	    cfw:fchar-right-junction ?┫
	    cfw:fchar-top-junction ?┯
	    cfw:fchar-top-left-corner ?┏
	    cfw:fchar-top-right-corner ?┓)
      ;; (setq cfw:render-line-breaker cfw:render-line-breaker-none)
      (setq cfw:org-overwrite-default-keybinding t)
      )
  (print "If you want nice calendars in orgmode, run  'git clone https://github.com/kiwanami/emacs-calfw " calfw-dir)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  General emacs settings 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

(setq visible-bell 1) ;;hey emacs: stfu!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Special orgmode settings 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;To keep the agenda fast, uncomment the following (but you'll need to comment it for reviews)
;;(setq org-agenda-span 'day)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  General orgmode settings 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;Some formatting that I like 
;;  from http://stackoverflow.com/questions/1771981/how-to-keep-indentation-with-emacs-org-mode-visual-line-mode
;(add-hook 'org-mode-hook (lambda () (visual-line-mode 1))) ;; wrap lines at the right margin
(add-hook 'org-mode-hook (lambda () (org-indent-mode t))) ;; start opened files indented
;;(add-hook 'org-mode-hook (lambda () (auto-fill-mode t))) ;; automatically split lines at right margin as you type

(visual-line-mode t)

;; Follow links on hitting enter
(setq org-return-follows-link t)

;; Don't allow hidden edits
(setq org-catch-invisible-edits 'error)


(setq org-hide-leading-stars nil)
(setq org-startup-indented t)
(setq org-cycle-separator-lines 0)

(setq org-blank-before-new-entry (quote ((heading)
                                         (plain-list-item . auto))))

(setq org-insert-heading-respect-content nil)

(setq org-reverse-note-order nil)

(setq org-show-following-heading t)
(setq org-show-hierarchy-above t)
(setq org-show-siblings (quote ((default))))

(setq org-special-ctrl-a/e t)
(setq org-special-ctrl-k t)
(setq org-yank-adjusted-subtrees t)

(setq org-id-method (quote uuidgen))

(setq org-deadline-warning-days 14)

(setq org-log-done (quote time))
(setq org-log-into-drawer t)
(setq org-log-state-notes-insert-after-drawers nil)

;; (setq org-time-clocksum-format
;;       '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))
(setq org-duration-format (quote h:mm))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  enable flyspell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ispell setup -- on windows this requires installing aspell in cygwin
;; note: I tried hunspell but could not get hunspell to find the dictionaries
                                        ;(custom-set-variables
                                        ; '(ispell-program-name "C:/cygwin64/bin/aspell.exe"))
(add-hook 'org-mode-hook (lambda () (flyspell-mode t)))

;;enable flyspell for text mode and tex files
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'TeX-mode-hook 'flyspell-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     startup and keybinding tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-startup-indented t)
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f6>") 'org-clock-in)
(global-set-key (kbd "<f7>") 'org-clock-out)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Directories and files for org-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;note: I'm deliberately setting globally accessible variables in a local scope so 
;;      a clear error will be produced for unknown machines or borked directories.

(print (system-name))

(if (gnus-string-equal (system-name) "u16main")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for personal ubuntu main")
    ;; add a single directory
    ;;(add-to-list 'org-agenda-files (expand-file-name"~/workspace/test/" ))      ;;add any org file in workspace/test

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
          (append
           (org-agenda-files)
           (file-expand-wildcards "~/workspace/*/*.org")      ;;any subdirectory of workspace
           ))
    (setq makro-personal-org-dir "~/workspace/org-personal-git/")
    (setq makro-diary-file  (concat makro-personal-org-dir "personal-diary.org"))
    (setq makro-refile-file (concat makro-personal-org-dir "personal-refile.org"))
    ))

(if (gnus-string-equal (system-name) "prelude")  ;;odd spacing for side-by-side comparison to PRELUDE setup
    (progn 
      (print "Loading setup for personal ubuntu main")
      ;; add a single directory
      ;;(add-to-list 'org-agenda-files (expand-file-name"~/workspace/test/" ))      ;;add any org file in workspace/test

      ;;add project-specific files or directories to org-agenda-files
      (setq org-agenda-files
            (append
             (org-agenda-files)
             (file-expand-wildcards "z:/files/workspace/*/*.org")      ;;any subdirectory of workspace
             ))
      (setq makro-personal-org-dir "z:/files/workspace/org-personal-git/")
      (setq makro-diary-file  (concat makro-personal-org-dir "personal-diary.org"))
      (setq makro-refile-file (concat makro-personal-org-dir "personal-refile.org"))
      ))

(if (gnus-string-equal (system-name) "LN14GFE40")  ;;odd spacing for side-by-side comparison to PRELUDE setup
  (progn 
    (print "Loading setup for linux work laptop")
    (setq makro-nrl-org-dir "~/workspace/org-nrl-git/")
    (setq org-directory makro-nrl-org-dir)

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
          (append
           (file-expand-wildcards "~/workspace/*/*.org")  ;;any subdir of workspace
           (file-expand-wildcards "~/workspace/papers/*/*.org") ;;any subdirectory of workspace/papers
           (file-expand-wildcards "~/workspace/projects/*/*.org") ;;any subdirectory of workspace/projects
           (file-expand-wildcards "~/workspace/proposals/*/*.org") ;;any subdirectory of workspace/proposals
           (file-expand-wildcards "~/workspace/workshops/*/*.org") ;;any subdirectory of workspace/workshops
           (file-expand-wildcards "~/workspace/reading/*/*.org") ;;any subdirectory of workspace/reading
           (file-expand-wildcards "~/workspace/travel/*-git/*.org") ;;any subdirectory of workspace/travel
           (file-expand-wildcards "~/workspace/reviewing/*-git/*.org") ;;any subdirectory of workspace/reviewing
           (file-expand-wildcards "~/workspace/actorsim/*-git/*.org") ;;any subdirectory of workspace/actorsim
           (file-expand-wildcards "~/workspace/talks/*/*.org") ;;any subdirectory of workspace/talks

           ;; (append
           ;;(org-agenda-files)
           ;; (file-expand-wildcards "~/workspace/actorsim.git/doc/*.org")
           ;;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*.org")) ;;within yaer
           ;;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*/*.org")) ;;within year/subproject
           ))
i

    ;; move the summary file to the front so items in that file show up first
    (print org-agenda-files)
    (setq project-file "~/workspace/projects/projects-general-git/sprint.org")
    (setq tmp-agenda-files (delete project-file org-agenda-files))
    (print "------Temporary agenda files")
    (print tmp-agenda-files)
    (setq org-agenda-files
          (append (list project-file) tmp-agenda-files))
    (print "------ Adjusted agenda files")
    (print org-agenda-files)


    (setq makro-diary-file  (concat makro-nrl-org-dir "nrl-diary.org"))
    (setq makro-refile-file (concat makro-nrl-org-dir "nrl-refile.org"))
    ))


;; (if (gnus-string-equal (system-name) "wn14mk")  ;;odd spacing for side-by-side comparison to 
;;(if (string-match-p (regexp-quote "wn14mk") system-name)   ;;odd spacing for side-by-side comparison to     PRELUDE setup
(if (string-match-p (regexp-quote "wn14gfe2104mr") system-name)   ;;odd spacing for side-by-side comparison to     PRELUDE setup
  (progn 
    (print "Loading setup for w10 SB3 work laptop")
    (setq makro-nrl-org-dir "~/workspace/org-nrl-git/")
    ;; (setq makro-nrl-org-dir "~/workspace/org-nrl-test/")
    (setq org-directory makro-nrl-org-dir)

    ;;add project-specific files or directories to org-agenda-files
    (setq org-agenda-files
          (append
           (file-expand-wildcards "~/workspace/*/*.org")  ;;any subdir of workspace
           (file-expand-wildcards "~/workspace/papers/*/*.org") ;;any subdirectory of workspace/papers
           (file-expand-wildcards "~/workspace/projects/*/*.org") ;;any subdirectory of workspace/projects
           (file-expand-wildcards "~/workspace/proposals/*/*.org") ;;any subdirectory of workspace/proposals
           (file-expand-wildcards "~/workspace/workshops/*/*.org") ;;any subdirectory of workspace/workshops
           (file-expand-wildcards "~/workspace/reading/*/*.org") ;;any subdirectory of workspace/reading
           (file-expand-wildcards "~/workspace/travel/*-git/*.org") ;;any subdirectory of workspace/travel
           (file-expand-wildcards "~/workspace/service*/*-git/*.org") ;;any subdirectory of workspace/reviewing
           (file-expand-wildcards "~/workspace/actorsim/*-git/*.org") ;;any subdirectory of workspace/actorsim
           (file-expand-wildcards "~/workspace/talks/*/*.org") ;;any subdirectory of workspace/talks

           ;; (append
           ;;(org-agenda-files)
           ;; (file-expand-wildcards "~/workspace/actorsim.git/doc/*.org")
           ;;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*.org")) ;;within yaer
           ;;(file-expand-wildcards (concat makro-dropbox-shared-research-org-dir "*/*/*.org")) ;;within year/subproject
           ))


    ;; move the summary file to the front so items in that file show up first
    (print org-agenda-files)
    (setq project-file "~/workspace/projects/projects-general-git/sprint.org")
    ;; (setq project-file (concat makro-nrl-org-dir "test.org"))
    (setq tmp-agenda-files (delete project-file org-agenda-files))
    (print "------Temporary agenda files")
    (print tmp-agenda-files)
    (setq org-agenda-files
          (append (list project-file) tmp-agenda-files))
    (print "------ Adjusted agenda files")
    (print org-agenda-files)


    (setq makro-diary-file  (concat makro-nrl-org-dir "nrl-diary.org"))
    (setq makro-refile-file (concat makro-nrl-org-dir "nrl-refile.org"))
    ))






(setq org-default-notes-file  makro-refile-file)
(setq org-agenda-diary-file makro-diary-file)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           todo tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-todo-keywords
  `((sequence 
     "TODO(t)" 
     "NEXT(n)" 
     "|" 
     "DONE(d)")

    (sequence
     "WAITING(w@/!)" 
     "HOLD(h@/!)" 
     "|" 
     "CANCELLED(c@/!)" 
     "PHONE" 
     "MEETING")))


;;to see a list of colors, type M-x list-colors-display
(setq org-todo-keyword-faces
  (quote (("TODO" :foreground "dark grey" :weight bold)
          ("NEXT" :foreground "dark green" :weight bold)
          ("DONE" :foreground "dark goldenrod" :weight bold)
          ("WAITING" :foreground "orange" :weight bold)
          ("HOLD" :foreground "tomato" :weight bold)
          ("CANCELLED" :foreground "red" :weight bold)
          ("MEETING" :foreground "purple" :weight bold)
          ("PHONE" :foreground "purple" :weight bold))))

(setq org-use-fast-todo-selection t)

(setq org-treat-S-cursor-todo-selection-as-state-change nil)

(setq org-todo-state-tags-triggers
  (quote (("CANCELLED" ("CANCELLED" . t))
          ("WAITING" ("WAITING" . t))
	  ("HOLD" ("WAITING") ("HOLD" . t))
	  (done ("WAITING") ("HOLD"))
	  ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
	  ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
	  ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

;;prevent tasks with unfinished subtasks from transitioning to done
(setq org-enforce-todo-dependencies t)

;; if you find you are un/checking checkboxes on repeating tasks, use the following
;; (add-to-list 'load-path (expand-file-name "~/git/org-mode/contrib/lisp"))
;; (require 'org-checklist)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           capture tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-capture-templates
  '(
    ("n" "note" entry (file makro-refile-file)
     "* %? :NOTE:\nCreated:%U\n%a\n" :clock-in nil :clock-resume nil)
    
    ("t" "todo" entry (file makro-refile-file)
     "* TODO %?\nCreated:%U\n\n" :clock-in nil :clock-resume nil)
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           refile tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (setq org-log-refile t)
;; (load-file "bh-refile-tweaks.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           link tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-confirm-shell-link-function 'y-or-n-p)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           tag tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Tags with fast selection keys
(setq org-tag-alist '(
          ("GO" . ?g)               ;;a task ready for work or something urgent 
          ("BACK_BURNER" . ?b)      ;;tasks to do when you're tired or in secondary meetings
	  ("SPRINT_16" . ?s)        ;;active SPRINT
	  ("SPRINT_15" . ?l)        ;;last SPRINT (to easily mark missed items)
	  ("SPRINT_TARGET" . ?t)    ;;subtasks to target this sprint
	  ("FOCUS" . ?f)            ;;subtasks to focus on if possible this sprint
	  ("THIS_SPRINT" . ?1)      ;;tasks for this sprint
	  ("NEXT_SPRINT" . ?2)      ;;tasks for the next sprint
	  ("SPRINT_BACKLOG" . ?3)   ;;backlogged projects & tasks
	  ("SPRINT_NOTE" . ?n)      ;;a note to show on sprint agendas - use for jump points
	  ("YAR" . ?y)              ;;things to report on your YAR -- Yearly Achievements Report

          ;;tags used to hide non-todo headlines inside a todo hierarchy
          ("DETAILS" . ?d)          ;;a details headline that isn't really a task or project
	  ("PROJECT" . ?p)          ;;a long-running headline or task for tracking subtasks and appointments

          ;;tags used to annotate a project or item is ready for a debrief and removal
	  ("ARCHIVE" . ?a)

          ;;tags used by TODO transitions -- not sure I need these... but they may icome in handy some day
	  ("WAITING" . ?w)
	  ("HOLD" . ?h)
	  ("CANCELLED" . ?c)

          ;;tags I rarely use and should probably remove
          ("QUAD_CURRENT" . ?4)
	  ("QUAD_NEXT" . ?5)
	  ("QUAD_BACKLOG" . ?6)
	  ("YEAR_2017" . ?7) ;; 1 year goals
	  ("YEAR_2019" . ?8) ;; 2 year goals
	  ("YEAR_2021" . ?9) ;; 5 year goals
	  ("YEAR_2026" . ?0) ;;10 year goals
	  ("YEAR_2036" . ?-) ;;20 year goals
	  ("VISION" . ?v)    ;;mission statements
	  ;("BOOKMARK" . ?b)         ;;quick access to links and notes
	  ("PROJECT_IDEAS" . ?q)    ;;project ideas (someday/maybe)
	  ("FLAGGED" . ??)
	  (:startgroup . nil)
	  ;; ("@online" . ?O)
	  ;; ("@errand" . ?E)
	  ;; ("@nrl" . ?N)
	  (:endgroup . nil)    ))

(setq org-tag-persistent-alist org-tag-alist)

;; For tag searches ignore tasks with scheduled and deadline dates
;(setq org-agenda-tags-todo-honor-ignore-options t)

;; Allow setting single tags without the menu
;; (setq org-fast-tag-selection-single-key (quote expert))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Timestamps
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar bh/insert-inactive-timestamp t)

(defun bh/toggle-insert-inactive-timestamp ()
  (interactive)
  (setq bh/insert-inactive-timestamp (not bh/insert-inactive-timestamp))
  (message "Heading timestamps are %s" (if bh/insert-inactive-timestamp "ON" "OFF")))

(defun bh/insert-inactive-timestamp ()
  (interactive)
  (org-insert-time-stamp nil t t nil nil nil))

(defun bh/insert-heading-inactive-timestamp ()
  (save-excursion
    (when bh/insert-inactive-timestamp
      (org-return)
      (org-cycle)
      (bh/insert-inactive-timestamp))))

(add-hook 'org-insert-heading-hook 'bh/insert-heading-inactive-timestamp 'append)

;; The following custom-set-faces create the highlights
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(org-mode-line-clock ((t (:background "grey75" :foreground "red" :box (:line-width -1 :style released-button)))) t))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Exporting 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-export-with-timestamps nil)

(setq org-table-export-default-format "orgtbl-to-csv")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Custom Agendas
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-agenda-start-day "10d")
;; ;(setq org-agenda-span 'day) ;;use if the agenda is loading very slowly

;; Do not dim blocked tasks
(setq org-agenda-dim-blocked-tasks nil)

;; Clean up the block agenda view
(setq org-agenda-compact-blocks t)
(setq org-agenda-remove-tags t)

;; Show more than just the local context from the egenda
(setq org-show-context-detail
      `((agenda . ancestors)
        (bookmark-jump . lineage)
        (isearch . lineage)
        (default . ancestors)))

;; Custom priorities
(setq org-highest-priority ?A)
(setq org-lowest-priority ?Z)
(setq org-default-priority ?J)


;; Custom agenda command definitions
;; a great tutorial is found at: http://orgmode.org/worg/org-tutorials/org-custom-agenda-commands.html
(setq org-agenda-custom-commands
      '(
	;; test agendas if something goes wrong
	;; ("x" agenda)
        ;; ("y" agenda*)
        ;; ("w" todo "WAITING")
	;; agendas no longer used -- remove eventually
	;; ("N" "Notes" tags "NOTE"
	;;  ((org-agenda-overriding-header "Notes")
	;;   (org-tags-match-list-sublevels t)))
	;; ("h" "Habits" tags-todo "STYLE=\"habit\""
	;;  ((org-agenda-overriding-header "Habits")
	;;   (org-agenda-sorting-strategy
	;;    '(todo-state-down effort-up category-keep))))
	;; ("r" "Refile" tags "REFILE"
	;;  ((org-agenda-overriding-header "Tasks to Refile")
	;;   (org-tags-match-list-sublevels t)))	
	("d" "Daily Agenda"
	 ((agenda "" ((org-agenda-span 1)))
	  ;; Go, Go, Go! (new format)
	  (tags-todo "+GO|+URGENT"
		     ((org-agenda-overriding-header "Today's tasks (+GO +URGENT)")
		      (org-tags-match-list-sublevels nil)
		      (org-agenda-sorting-strategy '(priority-down))))
	  ;; GO todos that need a quick review to close
	  (tags "+GO/DONE"
		((org-agenda-overriding-header "Finished, needs review (+TARGET TODO:DONE)")
		 (org-agenda-sorting-strategy '(priority-down))))
	  (tags "BACK_BURNER"
	  	((org-agenda-overriding-header "Back burner")
	  	 (org-tags-match-list-sublevels nil)))
	  ;; (tags "SPRINT_NOTE"
	  ;; 	((org-agenda-overriding-header "Sprint Notes")
	  ;; 	 (org-tags-match-list-sublevels nil)))
	  nil )) ;; end of Daily Agenda-----------------------------------------------
	("e" "Extended Daily Agenda"
	 ((agenda "" ((org-agenda-span 1)))
	  ;; Go, Go, Go! (new format)
	  (tags-todo "+GO|+URGENT"
		     ((org-agenda-overriding-header "Today's tasks (+GO +URGENT)")
		      (org-tags-match-list-sublevels nil)
		      (org-agenda-sorting-strategy '(priority-down))))
	  ;; GO todos that need a quick review to close
	  (tags "+GO/DONE"
		((org-agenda-overriding-header "Finished, needs review (+TARGET TODO:DONE)")
		 (org-agenda-sorting-strategy '(priority-down))))
	  ;; Todos in decreasing order (next, target, waiting)
	  (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET/!WAITING"
		     ((org-agenda-overriding-header "Waiting target actions (+TARGET TODO:WAITING)")
		      (org-agenda-sorting-strategy '(priority-down))))
	  (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET/!NEXT"
		     ((org-agenda-overriding-header "Next target actions (+TARGET TODO:NEXT)")
		      (org-agenda-sorting-strategy '(priority-down))))
	  (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET-DAILY/!-DONE-NEXT-WAITING"
		     ((org-agenda-overriding-header "Sprint Targets (+TARGET TODO:-DONE-NEXT-WAITING)")
		      (org-tags-match-list-sublevels nil)
		      (org-agenda-sorting-strategy '(priority-down))))
	  (tags-todo "-GO+THIS_SPRINT/!WAITING"
		     ((org-agenda-overriding-header "Sprint waiting (TODO:WAITING)")
		      (org-agenda-sorting-strategy '(category-keep))))
	  (tags "SPRINT_NOTE"
		((org-agenda-overriding-header "Sprint Notes")
		 (org-tags-match-list-sublevels nil)))
	  nil )) ;; end of Daily Agenda-----------------------------------------------
	("f" "Failsafe Weekly Daily Agenda (includes unfinished but scheduled items -7d)"
	 (
	  ;;(agenda "" ((org-agenda-span 1)))
	  ;; Go, Go, Go! (new format)
	  ;; (tags-todo "+GO|+URGENT"
	  ;; 	     ((org-agenda-overriding-header "Today's tasks (+GO +URGENT)")
	  ;; 	      (org-tags-match-list-sublevels nil)
	  ;; 	      (org-agenda-sorting-strategy '(priority-down))))
	  ;; GO todos that need a quick review to close
	  ;; (tags "+GO/DONE"
	  ;; 	((org-agenda-overriding-header "Finished, needs review (+TARGET TODO:DONE)")
	  ;; 	 (org-agenda-sorting-strategy '(priority-down))))
	  (tags-todo "TIMESTAMP<=\"<today>\"&TIMESTAMP>=\"<-7d>\""
		((org-agenda-overriding-header "Unfinished TODO but still scheduled")
		 (org-agenda-entry-types '(:timestamp))
		 ))
	  ;; (tags "SPRINT_NOTE"
	  ;; 	((org-agenda-overriding-header "Sprint Notes")
	  ;; 	 (org-tags-match-list-sublevels nil)))
	  nil )) ;; end of Daily Agenda-----------------------------------------------
	("p" "Project Agenda" 
	 ((tags "+SPRINT_TARGET-YAR-PROJECT-DETAILS/-TODO-NEXT-DONE-CANCELLED"
		((org-agenda-overriding-header "Hanging targets (+TARGET without TODO)--------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  (tags "SPRINT_NOTE"
		((org-agenda-overriding-header "Sprint Notes")
		 (org-tags-match-list-sublevels nil)))
	  (tags "+THIS_SPRINT+SPRINT_TARGET"
		((org-agenda-overriding-header "Sprint targets")
		 (org-agenda-sorting-strategy '(priority-down))
		 (org-tags-match-list-sublevels nil)  ))
	  (tags "THIS_SPRINT+FOCUS"
		((org-agenda-overriding-header "Current Sprint Focus Projects")
		 (org-tags-match-list-sublevels nil)))
	  (tags "THIS_SPRINT-FOCUS"
		((org-agenda-overriding-header "Current Sprint Touch Projects")
		 (org-tags-match-list-sublevels nil)))
	  (tags "NEXT_SPRINT"
		((org-agenda-overriding-header "Next Sprint Projects")
		 (org-tags-match-list-sublevels nil)))
	  (tags "SPRINT_BACKLOG"
		((org-agenda-overriding-header "Sprint Backlog Projects")
		 (org-tags-match-list-sublevels nil)))
	  (tags "PROJECT-FOCUS-THIS_SPRINT-NEXT_SPRINT-SPRINT_BACKLOG-{SPRINT_*}"
		((org-agenda-overriding-header "Unsorted Projects")
		 (org-tags-match-list-sublevels nil)))
	  (tags "ARCHIVE"
		((org-agenda-overriding-header "Projects or tasks ready to archive")
		 (org-tags-match-list-sublevels nil)))
	  (tags "PROJECT_IDEAS"
		((org-agenda-overriding-header "Project Ideas")
		 (org-tags-match-list-sublevels nil)))
	  nil)) ;; End of Project agenda ---------------------------------------------
	("y" "Annual Sprint Details" 
	 ((tags "+THIS_SPRINT+YAR"
		((org-agenda-overriding-header "THIS_SPRINT +YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  (tags "+NEXT_SPRINT+YAR"
		((org-agenda-overriding-header "NEXT_SPRINT +YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  (tags "+YAR-THIS_SPRINT-NEXT_SPRINT-SPRINT_01-SPRINT_02-SPRINT_03-SPRINT_04-SPRINT_05-SPRINT_06-SPRINT_07-SPRINT_08-SPRINT_09-SPRINT_10-SPRINT_11-SPRINT_12-SPRINT_13-SPRINT_14-SPRINT_15-SPRINT_16-SPRINT_17-SPRINT_18-SPRINT_19-SPRINT_20"
		((org-agenda-overriding-header "YAR outside of sprint")
		 (org-tags-match-list-sublevels nil) ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_16"
		((org-agenda-overriding-header "SPRINT_16 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_16-YAR"
		((org-agenda-overriding-header "-- SPRINT_16 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_15"
		((org-agenda-overriding-header "SPRINT_15 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_15-YAR"
		((org-agenda-overriding-header "-- SPRINT_15 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_14"
		((org-agenda-overriding-header "SPRINT_14 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_14-YAR"
		((org-agenda-overriding-header "-- SPRINT_14 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_13"
		((org-agenda-overriding-header "SPRINT_13 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_13-YAR"
		((org-agenda-overriding-header "-- SPRINT_13 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_12"
		((org-agenda-overriding-header "SPRINT_12 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_12-YAR"
		((org-agenda-overriding-header "-- SPRINT_12 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_11"
		((org-agenda-overriding-header "SPRINT_11 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_11-YAR"
		((org-agenda-overriding-header "-- SPRINT_11 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_10"
		((org-agenda-overriding-header "SPRINT_10 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_10-YAR"
		((org-agenda-overriding-header "-- SPRINT_10 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_09"
		((org-agenda-overriding-header "SPRINT_09 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_09-YAR"
		((org-agenda-overriding-header "-- SPRINT_09 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_08"
		((org-agenda-overriding-header "SPRINT_08 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_08-YAR"
		((org-agenda-overriding-header "-- SPRINT_08 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_07"
		((org-agenda-overriding-header "SPRINT_07 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_07-YAR"
		((org-agenda-overriding-header "-- SPRINT_07 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_06"
		((org-agenda-overriding-header "SPRINT_06 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_06-YAR"
		((org-agenda-overriding-header "-- SPRINT_06 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_05"
		((org-agenda-overriding-header "SPRINT_05 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_05-YAR"
		((org-agenda-overriding-header "-- SPRINT_05 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_04"
		((org-agenda-overriding-header "SPRINT_04 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_04-YAR"
		((org-agenda-overriding-header "-- SPRINT_04 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_03"
		((org-agenda-overriding-header "SPRINT_03 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_03-YAR"
		((org-agenda-overriding-header "-- SPRINT_03 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_02"
		((org-agenda-overriding-header "SPRINT_02 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_02-YAR"
		((org-agenda-overriding-header "-- SPRINT_02 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  ;;--------------------------------------------------------------------------
	  (tags "+YAR+SPRINT_01"
		((org-agenda-overriding-header "SPRINT_01 For YAR --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-use-tag-inheritance nil)
		 (org-tags-match-list-sublevels nil) ))
	  (tags "+SPRINT_01-YAR"
		((org-agenda-overriding-header "-- SPRINT_01 --------------------------")
		 (org-agenda-sorting-strategy '(category-keep))
		 (org-tags-match-list-sublevels nil)  ))
	  nil )) ;; end of YAR Agenda-----------------------------------------------
	))

(setq org-agenda-custom-commands-old
  '(("N" "Notes" tags "NOTE"
     ((org-agenda-overriding-header "Notes")
      (org-tags-match-list-sublevels t)))
    ("h" "Habits" tags-todo "STYLE=\"habit\""
     ((org-agenda-overriding-header "Habits")
      (org-agenda-sorting-strategy
       '(todo-state-down effort-up category-keep))))
    ("r" "Refile" tags "REFILE"
     ((org-agenda-overriding-header "Tasks to Refile")
      (org-tags-match-list-sublevels t)))
    ("j" "Justin's Agenda"
     ((agenda "" ((org-agenda-span 1)))
      (tags-todo "+GO"
            ((org-agenda-overriding-header "Today's tasks (+GO)")
             (org-tags-match-list-sublevels nil)
             (org-agenda-sorting-strategy '(priority-down)) ))
      ;; GO todos that need a quick review to close
      (tags "+GO/DONE"
            ((org-agenda-overriding-header "Finished, needs review (+TARGET TODO:DONE)")
             (org-agenda-sorting-strategy '(priority-down))))
      ;; Todos in decreasing order (next, target, waiting)
      (tags-todo "-GO/!NEXT"
                 ((org-agenda-overriding-header "Next actions (TODO:NEXT)")
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "-GO/!-DONE-NEXT"
                 ((org-agenda-overriding-header "All TODOs (TODO:-DONE-NEXT)")
                  (org-agenda-sorting-strategy '(priority-down))))
      ;; things to sort out as you go
      (tags "REFILE-PERSONAL"
            ((org-agenda-overriding-header "Refile Tasks (+REFILE)")
             (org-tags-match-list-sublevels t)))
      ;; sprint notes
      (tags "SPRINT_NOTE"
            ((org-agenda-overriding-header "Sprint Notes")
             (org-tags-match-list-sublevels nil)))
      nil
      ))
    ("d" "Today's Agenda"
     ((agenda "" ((org-agenda-span 1)))
      (tags-todo "+GO"
            ((org-agenda-overriding-header "Today's tasks (+GO)")
             (org-tags-match-list-sublevels nil)
             (org-agenda-sorting-strategy '(priority-down)) ))
      ;; GO todos that need a quick review to close
      (tags "+GO/DONE"
            ((org-agenda-overriding-header "Finished, needs review (+TARGET TODO:DONE)")
             (org-agenda-sorting-strategy '(priority-down))))
      ;; Todos in decreasing order (next, target, waiting)
      (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET/!WAITING"
                 ((org-agenda-overriding-header "Waiting target actions (+TARGET TODO:WAITING)")
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET/!NEXT"
                 ((org-agenda-overriding-header "Next target actions (+TARGET TODO:NEXT)")
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "-GO+THIS_SPRINT+SPRINT_TARGET-DAILY/!-DONE-NEXT-WAITING"
                 ((org-agenda-overriding-header "Sprint Targets (+TARGET TODO:-DONE-NEXT-WAITING)")
                  (org-tags-match-list-sublevels nil)
                  (org-agenda-sorting-strategy '(priority-down))))
      (tags-todo "-GO+THIS_SPRINT/!WAITING"
                 ((org-agenda-overriding-header "Sprint waiting (TODO:WAITING)")
                  (org-agenda-sorting-strategy '(category-keep))))
      ;; things to sort out as you go
      (tags "REFILE-PERSONAL"
            ((org-agenda-overriding-header "Refile Tasks (+REFILE)")
             (org-tags-match-list-sublevels t)))
      ;; sprint focus (that are not targets)
      (tags "THIS_SPRINT-GO+FOCUS"
            ((org-agenda-overriding-header "Sprint Focus (+FOCUS)")
             (org-tags-match-list-sublevels nil)))
      (tags "THIS_SPRINT-GO-SPRINT_TARGET-FOCUS"
            ((org-agenda-overriding-header "Sprint Touch Projects (-TARGET, -FOCUS)")
             (org-tags-match-list-sublevels nil)))
      (tags "SPRINT_NOTE"
            ((org-agenda-overriding-header "Sprint Notes")
             (org-tags-match-list-sublevels nil)))
      nil
      ))
    ("p" "Project Agenda" 
     ((tags "+SPRINT_TARGET-YAR-PROJECT-DETAILS/-TODO-NEXT-DONE-CANCELLED"
            ((org-agenda-overriding-header "Hanging targets (+TARGET without TODO)--------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      (tags "SPRINT_NOTE"
            ((org-agenda-overriding-header "Sprint Notes")
             (org-tags-match-list-sublevels nil)))
      (tags "+THIS_SPRINT+SPRINT_TARGET"
            ((org-agenda-overriding-header "Sprint targets")
             (org-agenda-sorting-strategy '(priority-down))
             (org-tags-match-list-sublevels nil)  ))
      (tags "THIS_SPRINT+FOCUS"
            ((org-agenda-overriding-header "Current Sprint Focus Projects")
             (org-tags-match-list-sublevels nil)))
      (tags "THIS_SPRINT-FOCUS"
            ((org-agenda-overriding-header "Current Sprint Touch Projects")
             (org-tags-match-list-sublevels nil)))
      (tags "NEXT_SPRINT"
            ((org-agenda-overriding-header "Next Sprint Projects")
             (org-tags-match-list-sublevels nil)))
      (tags "SPRINT_BACKLOG"
            ((org-agenda-overriding-header "Sprint Backlog Projects")
             (org-tags-match-list-sublevels nil)))
      (tags "PROJECT-FOCUS-THIS_SPRINT-NEXT_SPRINT-SPRINT_BACKLOG-{SPRINT_*}"
            ((org-agenda-overriding-header "Unsorted Projects")
             (org-tags-match-list-sublevels nil)))
      (tags "ARCHIVE"
            ((org-agenda-overriding-header "Projects or tasks ready to archive")
             (org-tags-match-list-sublevels nil)))
      (tags "PROJECT_IDEAS"
            ((org-agenda-overriding-header "Project Ideas")
             (org-tags-match-list-sublevels nil)))
      nil))
    ("y" "Annual Sprint Details" 
     ((tags "+THIS_SPRINT+YAR"
            ((org-agenda-overriding-header "THIS_SPRINT +YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      (tags "+NEXT_SPRINT+YAR"
            ((org-agenda-overriding-header "NEXT_SPRINT +YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      (tags "+YAR-THIS_SPRINT-NEXT_SPRINT-SPRINT_01-SPRINT_02-SPRINT_03-SPRINT_04-SPRINT_05-SPRINT_06-SPRINT_07-SPRINT_08-SPRINT_09-SPRINT_10-SPRINT_11-SPRINT_12-SPRINT_13-SPRINT_14-SPRINT_15-SPRINT_16-SPRINT_17-SPRINT_18-SPRINT_19-SPRINT_20"
            ((org-agenda-overriding-header "YAR outside of sprint")
             (org-tags-match-list-sublevels nil) ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_16"
            ((org-agenda-overriding-header "SPRINT_16 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_16-YAR"
            ((org-agenda-overriding-header "-- SPRINT_16 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_15"
            ((org-agenda-overriding-header "SPRINT_15 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_15-YAR"
            ((org-agenda-overriding-header "-- SPRINT_15 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_14"
            ((org-agenda-overriding-header "SPRINT_14 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_14-YAR"
            ((org-agenda-overriding-header "-- SPRINT_14 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_13"
            ((org-agenda-overriding-header "SPRINT_13 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_13-YAR"
            ((org-agenda-overriding-header "-- SPRINT_13 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_12"
            ((org-agenda-overriding-header "SPRINT_12 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_12-YAR"
            ((org-agenda-overriding-header "-- SPRINT_12 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_11"
            ((org-agenda-overriding-header "SPRINT_11 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_11-YAR"
            ((org-agenda-overriding-header "-- SPRINT_11 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_10"
            ((org-agenda-overriding-header "SPRINT_10 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_10-YAR"
            ((org-agenda-overriding-header "-- SPRINT_10 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_09"
            ((org-agenda-overriding-header "SPRINT_09 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_09-YAR"
            ((org-agenda-overriding-header "-- SPRINT_09 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_08"
            ((org-agenda-overriding-header "SPRINT_08 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_08-YAR"
            ((org-agenda-overriding-header "-- SPRINT_08 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_07"
            ((org-agenda-overriding-header "SPRINT_07 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_07-YAR"
            ((org-agenda-overriding-header "-- SPRINT_07 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_06"
            ((org-agenda-overriding-header "SPRINT_06 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_06-YAR"
            ((org-agenda-overriding-header "-- SPRINT_06 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_05"
            ((org-agenda-overriding-header "SPRINT_05 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_05-YAR"
            ((org-agenda-overriding-header "-- SPRINT_05 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_04"
            ((org-agenda-overriding-header "SPRINT_04 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_04-YAR"
            ((org-agenda-overriding-header "-- SPRINT_04 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_03"
            ((org-agenda-overriding-header "SPRINT_03 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_03-YAR"
            ((org-agenda-overriding-header "-- SPRINT_03 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_02"
            ((org-agenda-overriding-header "SPRINT_02 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_02-YAR"
            ((org-agenda-overriding-header "-- SPRINT_02 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      ;;--------------------------------------------------------------------------
      (tags "+YAR+SPRINT_01"
            ((org-agenda-overriding-header "SPRINT_01 For YAR --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-use-tag-inheritance nil)
             (org-tags-match-list-sublevels nil) ))
      (tags "+SPRINT_01-YAR"
            ((org-agenda-overriding-header "-- SPRINT_01 --------------------------")
             (org-agenda-sorting-strategy '(category-keep))
             (org-tags-match-list-sublevels nil)  ))
      nil))
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Agenda view tweaks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Overwrite the current window with the agenda
(setq org-agenda-window-setup 'current-window)

;; Always highlight the current agenda line
(add-hook 'org-agenda-mode-hook
          '(lambda () (hl-line-mode 1))
          'append)

;; Keep tasks with dates on the global todo lists
(setq org-agenda-todo-ignore-with-date t)

;; Keep tasks with deadlines on the global todo lists
(setq org-agenda-todo-ignore-deadlines t)

;; Keep tasks with scheduled dates on the global todo lists
(setq org-agenda-todo-ignore-scheduled t)

;; Keep tasks with timestamps on the global todo lists
(setq org-agenda-todo-ignore-timestamp t)

;; Remove completed deadline tasks from the agenda view
(setq org-agenda-skip-deadline-if-done t)

;; Remove completed scheduled tasks from the agenda view
(setq org-agenda-skip-scheduled-if-done t)

;; Remove completed items from search results
(setq org-agenda-skip-timestamp-if-done t)

;; Diary file not used
(setq org-agenda-include-diary nil)
;;ensure any time strings on heading show in the agenda
(setq org-agenda-insert-diary-extract-time t) 

;; Include agenda archive files when searching for things
(setq org-agenda-text-search-extra-files (quote (agenda-archives)))

;; Show all future entries for repeating tasks
(setq org-agenda-repeating-timestamp-show-all t)

;; Show all agenda dates - even if they are empty
(setq org-agenda-show-all-dates t)

;; Sorting order for tasks on the agenda
(setq org-agenda-sorting-strategy
      (quote ((agenda habit-down time-up user-defined-up effort-up category-keep)
              (todo category-up effort-up)
              (tags category-up effort-up)
              (search category-up))))

;; Start the weekly agenda on Monday
(setq org-agenda-start-on-weekday 0)

;; ;; Turn on the time grid
;; (setq org-agenda-use-time-grid t)

;; ;; Enable display of the time grid so we can see the marker for the current time
;; (setq org-agenda-time-grid (quote ((daily today remove-match)
;;                                    #("----------------" 0 16 (org-heading t))
;;                                    (0900 1100 1300 1500 1700))))

;; Display tags farther right
(setq org-agenda-tags-column -102)

;;
;; Agenda sorting functions
;;
(setq org-agenda-cmp-user-defined 'bh/agenda-sort)

(defun bh/agenda-sort (a b)
  "Sorting strategy for agenda items.
Late deadlines first, then scheduled, then non-late deadlines"
  (let (result num-a num-b)
    (cond
     ; time specific items are already sorted first by org-agenda-sorting-strategy

     ; non-deadline and non-scheduled items next
     ((bh/agenda-sort-test 'bh/is-not-scheduled-or-deadline a b))

     ; deadlines for today next
     ((bh/agenda-sort-test 'bh/is-due-deadline a b))

     ; late deadlines next
     ((bh/agenda-sort-test-num 'bh/is-late-deadline '> a b))

     ; scheduled items for today next
     ((bh/agenda-sort-test 'bh/is-scheduled-today a b))

     ; late scheduled items next
     ((bh/agenda-sort-test-num 'bh/is-scheduled-late '> a b))

     ; pending deadlines last
     ((bh/agenda-sort-test-num 'bh/is-pending-deadline '< a b))

     ; finally default to unsorted
     (t (setq result nil)))
    result))

(defmacro bh/agenda-sort-test (fn a b)
  "Test for agenda sort"
  `(cond
    ; if both match leave them unsorted
    ((and (apply ,fn (list ,a))
          (apply ,fn (list ,b)))
     (setq result nil))
    ; if a matches put a first
    ((apply ,fn (list ,a))
     (setq result -1))
    ; otherwise if b matches put b first
    ((apply ,fn (list ,b))
     (setq result 1))
    ; if none match leave them unsorted
    (t nil)))

(defmacro bh/agenda-sort-test-num (fn compfn a b)
  `(cond
    ((apply ,fn (list ,a))
     (setq num-a (string-to-number (match-string 1 ,a)))
     (if (apply ,fn (list ,b))
         (progn
           (setq num-b (string-to-number (match-string 1 ,b)))
           (setq result (if (apply ,compfn (list num-a num-b))
                            -1
                          1)))
       (setq result -1)))
    ((apply ,fn (list ,b))
     (setq result 1))
    (t nil)))

(defun bh/is-not-scheduled-or-deadline (date-str)
  (and (not (bh/is-deadline date-str))
       (not (bh/is-scheduled date-str))))

(defun bh/is-due-deadline (date-str)
  (string-match "Deadline:" date-str))

(defun bh/is-late-deadline (date-str)
  (string-match "\\([0-9]*\\) d\. ago:" date-str))

(defun bh/is-pending-deadline (date-str)
  (string-match "In \\([^-]*\\)d\.:" date-str))

(defun bh/is-deadline (date-str)
  (or (bh/is-due-deadline date-str)
      (bh/is-late-deadline date-str)
      (bh/is-pending-deadline date-str)))

(defun bh/is-scheduled (date-str)
  (or (bh/is-scheduled-today date-str)
      (bh/is-scheduled-late date-str)))

(defun bh/is-scheduled-today (date-str)
  (string-match "Scheduled:" date-str))

(defun bh/is-scheduled-late (date-str)
  (string-match "Sched\.\\(.*\\)x:" date-str))

;; Use sticky agenda's so they persist
(setq org-agenda-sticky t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Clocking Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Resume clocking task when emacs is restarted
(org-clock-persistence-insinuate)
;; Show lot of clocking history so it's easy to pick items off the C-F11 list
(setq org-clock-history-length 23)
;; Resume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)
;; Change tasks to NEXT when clocking in
(setq org-clock-in-switch-to-state 'mr/clock-in-to-next)
;; Remove clocking entries with 0:00 duration; can occur if you switch clocking on tasks quickly
(setq org-clock-out-remove-zero-time-clocks t)
;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
(setq org-clock-persist t)
;; Do not prompt to resume an active clock (not sure what this does yet..)
                                        ;(setq org-clock-persist-query-resume nil)
;; Enable auto clock resolution for finding open clocks
(setq org-clock-auto-clock-resolution (quote when-no-clock-is-running))
;; Include current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)
;;force time editing to use discrete minute intervals (no rounding):
(setq org-time-stamp-rounding-minutes (quote (1 1)))

(defun mr/clock-in-to-next (kw)
  "Switch a task from TODO to NEXT when clocking in, skipping capture tasks."
  (when (not (and (boundp 'org-capture-mode) org-capture-mode))
    (cond
     ((member (org-get-todo-state) (list "TODO"))
      "NEXT") )))

(setq org-agenda-clock-consistency-checks
      (quote (:max-duration "4:00"
                            :min-duration 0
                            :max-gap 0
                            :gap-ok-around ("4:00"))))

;; Agenda clock report parameters
(setq org-agenda-clockreport-parameter-plist
      (quote (:link t :maxlevel 5 :fileskip0 t :compact t :formula %)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Effort Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set default column view headings: Task Effort Clock_Summary
;;(setq org-columns-default-format "%80ITEM(Task) %10Effort(Effort){:} %10CLOCKSUM")

;; global Effort estimate values
;; global STYLE property values for completion
;; (setq org-global-properties (quote (("Effort_ALL" . "0:15 0:30 0:45 1:00 2:00 3:00 4:00 5:00 6:00 0:00")
;;                                    ("STYLE_ALL" . "habit"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;           Print debugging details and declare Success!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(print org-tag-alist)
(print "Loaded Custom Org Setup - Success!")


 
