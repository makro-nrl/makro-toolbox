#!/bin/sh

#on ubuntu 16.04, install emacs25 by
# sudo add-apt-repository ppa:kelleyk/emacs
# sudo apt update
# sudo apt install emacs25

export EMACS_USER_DIRECTORY=~/.spacemacs.d

exec emacs26 "$@"
#to debug startup or init load time, use the following
exec emacs26 --debug-init --timed-requires "$@"
