
The scripts in this directory allow emacs and spacemacs to run simultaneously on the same system.  

- Install emacs26 for spacemacs
  - sudo add-apt-repository ppa:kelleyk/emacs
  - sudo apt update
  - sudo apt install emacs26

- Install spacemacs by cloning it:

  '''git clone https://github.com/syl20bnr/spacemacs ~/.spacemacs.d'''

  - if spacemacs was previously installed, *move* ~/.emacs.d to ~/.spacemacs.d 

- Link the script spacemacs.sh to ~/spacemacs.sh
- Line the dot-emacs-with-user-dir-switch to ~/.emacs
- Link the dot-spacemacs config file to ~/.spacemacs
- Link the spacemacs-org.el to ~/spacemacs-org.el
- Run ~/spacemacs.sh and ensure spacemacs works properly
- Run emacs for the first time on the now non-existent .emacs_d directory
- If on ubuntu link the desktop file

If on windows and using WSL
===========================

Install powerline fonts:
------------------------
- open an admin powershell and type `powershell -ExecutionPolicy Bypass` (this allows you to run scripts)
- git clone https://github.com/powerline/fonts.git powerline-fonts-git
- cd powerline-fonts-git
- .\install.ps1
