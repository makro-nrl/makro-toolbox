#a simple prompt that is fast for WSL


# disable the prompt change from a <virtualenv>/bin/activate
export VIRTUAL_ENV_DISABLE_PROMPT=1

local date="$fg[green]%T"
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
local full_folder="%{$fg[cyan]%}%d"

local computer_and_login_name="$fg[white]$bg[black]%m"
hostname=`hostname`
if [[ $hostname = 'wn14gfe2104mr' ]]; then 
   local hostname= local computer_and_login_name="$fg[cyan]SB3"
elif [[ $hostname = 'beeblebrox' ]]; then
   local computer_and_login_name="$fg[white]$bg[blue]%m"
elif [[ $hostname = 'l14mk.aic.nrl.navy.mil' ]]; then
   local computer_and_login_name="$fg[white]$bg[red]%m"
fi

# From https://unix.stackexchange.com/questions/656045/how-to-modify-the-anaconda-environment-prompt-in-zsh
# Determines prompt modifier if and when a conda environment is active
# Also, you need to disable the default conda prompt using the following command
# conda config --set changeps1 false
precmd_conda_info() {
  if [[ $CONDA_DEFAULT_ENV = 'base' ]]; then
    # When no conda environment is active, don't show anything
    CONDA_ENV=""
  else
    CONDA_ENV="($CONDA_DEFAULT_ENV)"
  fi
}
precmd_functions+=( precmd_conda_info )


PROMPT='┏${date} ${full_folder}%{$reset_color%}
┣${CONDA_ENV}$(virtualenv_prompt_info)$(git_prompt_info)
┗${computer_and_login_name}%{$reset_color%} ${ret_status} %{$reset_color%}'
 
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[blue]%}(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
