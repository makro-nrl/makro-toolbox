#+STARTUP: indent overview

* Extracting annotations
** Research on extracting annotations
[2016-08-26 Fri 09:53]
In a quick web search, it appears there are three viable options:
- [[http://zotfile.com/][ZotFile]]: an extension to Zotero 
  But I recall this had issues before and I don't much like the idea of complicating my install
- [[https://github.com/rschroll/prsannots][PRSAnnots]]: A tool to pull annotations from eReader files
  Hmm... interesting but I don't use an eReader.  This looks like it could be useful in the future but may take a bit of customization
- [[https://www.sumnotes.net/][SumNotes]]: A cloud-based application that you can drag-n-drop files
  Cloud-based solutions are not an option for DoD work!
- [[https://en.wikipedia.org/wiki/Poppler_(software)][Poppler]]: a toolkit that has [[https://launchpad.net/poppler-python][python]] bindings
  This is more interesting!
- [[https://gist.github.com/compleatang/8666561][Scripts on GitHub]]: simple scripts built on top of Poppler
  Perfect!  Hopefully this works.  Boy would this save me a TON of work for reviews.  
* Printing 2up
pdfjam --nup 2x1 --suffix 2up --landscape --scale 1.10 --outfile . --outfile . [filename]

pdfjam --nup 2x1 --suffix 2up --landscape --scale 1.20 --offset '0cm 0.5cm' --outfile . --outfile . [filename]

[[http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam][pdfjam docs]]
Alternative: [[http://multivalent.sourceforge.net/][Multivalent]]

* Stitching together PDFs
Use
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOutputFile=finished.pdf  file1.pdf file2.pdf
