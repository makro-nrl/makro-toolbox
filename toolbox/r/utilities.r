if( Sys.getenv("HOST") == "finale") { 
  source("C:/cygwin/home/mak/files/public_html/toolbox/r/RColorBrewer/R/ColorBrewer.R")
} else {
  source("/s/chopin/k/grad/mroberts/public_html/toolbox/r/RColorBrewer/R/ColorBrewer.R")
}

colorMap <- c("1", "2", "#008B00", "4", "5", "6", "#FFB90F", "#8DD3C7", "#FB8072", "#80B1D3", "#CDCD00", "#B3DE69", "#FCCDE5", "#8C8C8C", "#BC80BD", "#CCEBC5", "#BEBADA", "#FDB462", "#D9D9D9", "#FF7F24")

## put histograms on the diagonal
panel.hist <- function(x, ...)
{
  usr <- par("usr"); on.exit(par(usr))
  par(usr = c(usr[1:2], 0, 1.5) )
  h <- hist(x, plot = FALSE)
  breaks <- h$breaks; nB <- length(breaks)
  y <- h$counts; y <- y/max(y)
  rect(breaks[-nB], 0, breaks[-1], y, col="black", ...)
}

## put correlations on the upper panels,
## with size proportional to the correlations.
panel.cor <- function(x, y, digits=2, prefix="", cex.cor)
  {
    usr <- par("usr"); on.exit(par(usr))
    par(usr = c(0, 1, 0, 1))
    r <- abs(cor(x, y))
    txt <- format(c(r, 0.123456789), digits=digits)[1]
    txt <- paste(prefix, txt, sep="")
    if(missing(cex.cor)) cex <- 0.8/strwidth(txt)
    text(0.5, 0.5, txt, cex = cex * r)
  }


##From http://personality-project.org/r/r.graphics.html, accessed 10/24/2005
##Version of November 26, 2004
##Author: William Revelle
##        Department of Psychology
##        Northwestern University
## TO USE THIS FUNCTION type:
##   pairs(data, lower.panel=panel.cor, upper.panel=panel.smooth)
##
## the following code and figure is adapted from the help file for
## pairs put (absolute) correlations on the lower panels, with size
## proportional to the correlations.  
panel.cor <- function(x, y, digits=2, prefix="", cex.cor)   {
  usr <- par("usr"); on.exit(par(usr))
  par(usr = c(0, 1, 0, 1))
  r = (cor(x, y))
  txt <- format(c(r, 0.123456789), digits=digits)[1]
  txt <- paste(prefix, txt, sep="")
  if(missing(cex.cor)) cex <- 0.8/strwidth(txt)
  text(0.5, 0.5, txt, cex = cex * abs(r))
}

##Author: Mark Roberts
##        Colorado State University
##
## TO USE THIS FUNCTION type:
##   pairs(data, lower.panel=panel.smooth, upper.panel=panel.color)
#panel.cor <- function(x, y, digits=2, prefix="", cex.cor)   {
#  usr <- par("usr"); on.exit(par(usr))
#  par(usr = c(0, 1, 0, 1))
# txt <- format(c(r, 0.123456789), digits=digits)[1]
#  txt <- paste(prefix, txt, sep="")
#  if(missing(cex.cor)) cex <- 0.8/strwidth(txt)
#  text(0.5, 0.5, txt, cex = cex * abs(r))
#}


##From http://personality-project.org/r/r.graphics.html, accessed 10/24/2005
##Version of November 26, 2004
##Author: William Revelle
##        Department of Psychology
##        Northwestern University
##Author: Mark Roberts
##        Colorado State University
##
## TO USE THIS FUNCTION type:
##   multi.hist(data)
## or 
##  multi.hist(data[,c(x,y,z)]) # to skip non-numeric columns
multi.hist <- function(x){
  nvar <- dim(x)[2]                   #number of variables
  nsize=trunc(sqrt(nvar))+1           #size of graphic
  old.par <- par(no.readonly = TRUE)  # all par settings which can be changed
  par(mfrow=c(nsize,nsize))           #set new graphic parameters
  for (i in 1:nvar) {
    name=names(x)[i]                  #get the names for the variables
    hist(x[,i],main=name,xlab=name) } #draw the histograms for each variable
  on.exit(par(old.par))               #set the graphic parameters back to the original
}


##From the R FAQ
labels.rotateX <- function() {
  ## Increase bottom margin to make room for rotated labels
  par(mar = c(7, 4, 4, 2) + 0.1)

  ## Create plot and get bar midpoints in 'mp'
  mp <- barplot(1:10)

  ## Set up x axis with tick marks alone
  axis(1, at = mp, labels = FALSE)

  ## Create some text labels
  labels <- paste("Label", 1:10, sep = " ")

  ## Plot x axis labels at mp
  text(mp, par("usr")[3] - 0.5, srt = 45, adj = 1, labels = labels, xpd = TRUE)

  ## Plot x axis label at line 4
  mtext(1, text = "X Axis Label", line = 4)
}


##From the R Help Archive: Subject "Re: [R] How to rotate the axisnames in a BARPLOT"
##Author:Francisco J. Zagmutt <gerifalte28_at_hotmail.com> 
##Posted: Fri 01 Jul 2005 - 08:25:53 EST
##He states: Try the following. I made this based on the eaxmple from FAQ 7.27
##
##
labels.rotateX2 <- function() {
  tab=table(rpois(100, 2)) #Creates table to make a barplot
  par(mar = c(6, 4, 4, 2) + 0.1)#Prepares margin to fit x axis labels
  pts=barplot(tab, xaxt = "n", xlab = "", col="yellow2")#Barplot of tab without x axis or label.
                                                        #Also stores the middle points of the bars

  axis(side=1,at=pts, labels=F, tick=T)#Creates x axis with tickmarks exactly at the middle of the bars
  nam=paste("Text",names(tab))#Names of each category. To be used on labels

  text(pts, par("usr")[3] - 1.5, srt = 45, adj = 1, labels = nam, xpd = TRUE)

                                        #Adds the tickmark labels
                                        # adj = 1 will place the text at the end ot the tick marks
                                        # xpd = TRUE will "clip" text outside the plot region

  mtext(1, text = "My Axis Label", line = 4) #Adds x axis label at line 2
}



#Ryan Forbes figured out how do the same thing for boxplots
myboxplot <- function(a, formula=mae~neighborhood+similarity){
  par(mar = c(6, 4, 4, 2) + 0.1)
  pts=boxplot(formula, a, xaxt="n",xlab="", notch=T)
  text(1:length(pts$names), par("usr")[3] - 0.005, srt=60, adj=1,labels=pts$names,xpd=TRUE)
}

createSummaryMatrix <- function(data, round=2) {
    #NB: the apply function takes a 2 so it does the mean over the columns
    m <- apply( data, 2, mean)
    med <- apply( data, 2, median)
    s <- apply( data, 2, sd)
    low <- apply( data, 2, min)
    high <- apply( data, 2, max)
    data.summary <- round(matrix(c(m,s,med,low,high),ncol=5), round)
    dimnames(data.summary) <- list( names(data), c("mean","sd","med","min","max"))
    data.summary
    #latex.table(data.summary,caption="Summary statistics for ....")
}

