
import numpy as np
import matplotlib.pyplot as plt

np.set_printoptions(precision=4)
np.set_printoptions(suppress=True)

# GENERATE DATA
trial_num = 100
signal_length = 1000
X = np.linspace(0,1,signal_length)
Ys = np.sin(2*np.pi*X) + np.random.uniform(-.2, .2, (trial_num, signal_length)) 

Ymu = np.mean(Ys,0)
err = np.std(Ys,0)#/np.sqrt(trial_num-1)

# PLOT
plt.plot(X, Ymu, color=[0.0, 0.7, 0.5], alpha=0.8, label='Curve')
plt.fill_between(X, Ymu-err, Ymu+err, alpha=0.5, edgecolor='none', facecolor=[0.0, 0.7, 0.5])

plt.xlabel('X', fontsize=18)
plt.ylabel('Y', fontsize=18)
plt.axis([0,1,-2,2])
plt.yticks(np.arange(-2, 2.1, .5))
plt.xticks([])

plt.legend(loc='best') #'lower center' 'upper right' numpoints=1
plt.show()

