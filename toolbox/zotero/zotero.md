
Zotero Notes
=========================



ZotFile Notes
===============

The ZotFile plugin provides useful PDF naming help.

Adding Custom Renaming
------------------------------------

Details at http://zotfile.com/index.html#renaming-rules

Adapted from: https://forums.zotero.org/discussion/84520/zotfile-customize-journal-abbreviation-to-rename-files

A handy tool for reshaping jsom: https://jsoncompare.com/#!/simple/reformat=minify/

The following will rename journal and conference entries with their first letters



{"1":{"field":"publicationTitle","operations":[{"function":"replace","regex":"Proceedings|of|the|and|on","replacement":""},{"function":"replace","regex":"\\B[a-zA-Z]","replacement":""},{"function":"replace","regex":" ","replacement":""}]}}