
# Table of Contents

1.  [Getting started (creating a new repo)](#orge6c2113)
    1.  [Creating a new repo to share from a server (remember to use &#x2013;shared=group!)](#orge81c9bc)
2.  [Git resources](#orga6a3ce1)
    1.  [Read through and point to relevant sections of each of these resources in your getting started guide.](#orgbc9e327)
    2.  [Git Magic by Ben Flynn](#orgb9e70be)
    3.  [Add the following resource links](#orgae22bb8)
        1.  [Pro Git](#orgefe68eb)
        2.  [Git Internals](#orgcff2b0d)
        3.  [Git Guys - ADD description](#org7b3e018)
        4.  [Resident Survey by Evan Misshula](#orgd6cf6f6)
3.  [Standardized commit messages](#org5543878)
4.  [Pushing an existing repo to BitBucket](#org451bba9)
5.  [Setting up ssh access to BitBucket](#org9c8f836)
    1.  [Cygwin](#orge6397f9)
    2.  [GitBash shell](#org8ae8167)
6.  [Common gotchas](#org24c9bd1)
    1.  [Ooops, I have been working on the wrong branch](#orgb44591c)
7.  [Using stash](#org487b62d)
    1.  [Restoring a lost stash](#org8c70f0d)
8.  [Linking a github repo to bitbucket](#orgba7c7a5)
9.  [Using git submodule](#org8412823)
10. [Refactoring git repos](#orgd4cda44)
    1.  [My notes](#org4861821)
    2.  [Using git-filter-repo](#orgbc753a1)
    3.  [[deprecated] Quoted From: May, 2011 article by Greg Bayer](#org23fae22)
11. [Fixing a remote that has incorrect commits](#orgc22065e)
12. [Setting the upstream remote for a branch](#org3889197)
13. [Removing a directory and its history](#org1034c84)
14. [Fixing permissions issues after moving directories](#orgdf0a1da)
15. [Creating a git bundle and zip (useful for sofware release)](#org161137a)


<a id="orge6c2113"></a>

# Level up your git
[https://ohmygit.org/](https://ohmygit.org/)


# Getting started (creating a new repo)


<a id="orge81c9bc"></a>

## Creating a new repo to share from a server (remember to use &#x2013;shared=group!)

-   using unix commands
    git init &#x2013;bare &#x2013;shared=group dirname.git
    chgrp grpname -R dirname.git
    chmod g+s -R dirname.git

-   using git shared option
    git init &#x2013;bare &#x2013;shared=group dirname.git
    chgrp grpname -R dirname.git


<a id="orga6a3ce1"></a>

# Git resources


<a id="orgbc9e327"></a>

## TODO Read through and point to relevant sections of each of these resources in your getting started guide.

<span class="timestamp-wrapper"><span class="timestamp">[2016-01-22 Fri 15:05]</span></span>


<a id="orgb9e70be"></a>

## [Git Magic](http://www-cs-students.stanford.edu/~blynn/gitmagic/) by Ben Flynn


<a id="orgae22bb8"></a>

## TODO Add the following resource links


<a id="orgefe68eb"></a>

### Pro Git


<a id="orgcff2b0d"></a>

### Git Internals


<a id="org7b3e018"></a>

### [Git Guys](http://www.gitguys.com/) - ADD description


<a id="orgd6cf6f6"></a>

### [Resident Survey](https://github.com/EvanMisshula/residentSurvey) by Evan Misshula


<a id="org5543878"></a>

# Standardized commit messages

-   use a succinct first-line summar so a one-line per commit history will be meaningful
-   use a branch for any change
-   try to keep branches small and short-lived
-   merge branches back to master once approved
-   separate commits of substantive code changes and formatting/documentation changes


<a id="org451bba9"></a>

# Pushing an existing repo to BitBucket

This one took quite a while to figure out, but was on the atlasssian support page all along: [web](https://confluence.atlassian.com/bitbucket/import-code-from-an-existing-project-259358821.html)
The short version: 
  (Set up ssh keys if you haven't already)
  Create an empy repo.git on your bitbucket account
  Open a GitBash shell
  move to the local repo directory
  Type: git push &#x2013;mirror git@bitbucket.org:username/repo.git

This will mirror the repo on bitbucket and then you can start using it.

Remember to push/pull from this repo too!


<a id="org9c8f836"></a>

# Setting up ssh access to BitBucket

The most helpful page with regard to setting this up for tortoiseSVN is [here](http://guganeshan.com/blog/setting-up-git-and-tortoisegit-with-bitbucket-step-by-step.html).  The official documentation is overly complex but the [overview](https://confluence.atlassian.com/bitbucket/use-the-ssh-protocol-with-bitbucket-cloud-221449711.html)  is helpful as well as the description of setting up an SSH key, [here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).  You can access the latest instructions by looking in your profile settings under SSH keys.

What is required depends on whether you are using tortoise+cygwin or gitbash.  
To create a identity key, use: 

ssh-keygen -t rsa -C "email@domain.com"

where email@domain.com is the registered email for your BB account. If you leave off the email the key may not work because it may lack an email.

Once you add the key to BB (see below), you can Verify the key is set up properly by running

ssh -T git@bitbucket.org


<a id="orge6397f9"></a>

## Cygwin

In a cygwin terminal, type the keygen command then copy the id<sub>rsa.pub</sub> file to your BB account
In Tortoise, modify the setting Network->SSH Client to "c:\cygwin64\bin\ssh.exe"

You may need to change the client from TortoisPlink: C:\Program Files\TortoiseGit\bin\TortoiseGitPlink.exe

This doesn't seem to cause any major problems&#x2026; 


<a id="org8ae8167"></a>

## GitBash shell

Type the keygen command and copy the 
In Tortoise, modify the setting Network->SSH Client to "ssh.exe"
You may need to verify that GitBash is on your path


<a id="org24c9bd1"></a>

# Common gotchas


<a id="orgb44591c"></a>

## Ooops, I have been working on the wrong branch

Key idea: git will try to merge changes as you switch depending
no conflicts case:
  \`\`git checkout new-branch\`\` will automatically merge
   git will output a "M" for merged files if there were any

conflicts case:
  \`\`git checkout new-branch\`\` will fail if changes are in conflict
  \`\`git checkout -m new-branch\`\` will force a merge for conflicts
   git will output a "M" for merged/conflicted files if there were any
   you must manually merge conflicts before committing

See discussion by the [Git Guys](http://www.gitguys.com/topics/switching-branches-without-committing/) on switching brances without committing


<a id="org487b62d"></a>

# Using stash


<a id="org8c70f0d"></a>

## Restoring a lost stash

-   From <http://stackoverflow.com/questions/89332/how-to-recover-a-dropped-stash-in-git#91795>
    Run the following command and look in the gui for your lost stash
    TOTAL LIFESAVER!!!
    gitk &#x2013;all $( git fsck &#x2013;no-reflog | awk '*dangling commit* {print $3}' )


<a id="orgba7c7a5"></a>

# Linking a github repo to bitbucket

<https://gist.github.com/jcaraballo/1982605>


<a id="org8412823"></a>

# Using git submodule

<http://blogs.atlassian.com/2013/03/git-submodules-workflows-tips/>


<a id="orgd4cda44"></a>

# Refactoring git repos


<a id="org4861821"></a>

## My notes

I used to use git-filter-branch but it seems a newer tool called git-filter-repo is more powerful and can replace BFG as well.


<a id="orgbc753a1"></a>

## Using git-filter-repo

In source repo: 

-   git checkout -b <merge-branch-name>
-   git filter-repo &#x2013;path src/ &#x2013;to-subdirectory-filter my-module

In destination repo:

-   git clone <destination repo> destination-git
-   cd destination-git
-   git remote add tmp-source <directory to source repo>
-   git pull tmp-source
-   git checkout <merge-branch-name> 
    -   double check contents are what you expect
-   git checkout develop
-   git merge <merge-branch-name> &#x2013;allow-unrelated-histories
-   git push <actual remote> develop


<a id="org23fae22"></a>

## [deprecated] Quoted From: May, 2011 [article](http://gbayer.com/development/moving-files-from-one-git-repository-to-another-preserving-history/) by Greg Bayer

Get files ready for the move:

Make a copy of repository A so you can mess with it without worrying
about mistakes too much.  It's also a good idea to delete the link to
the original repository to avoid accidentally making any remote
changes (line 3).  Line 4 is the critical step here.  It goes through
your history and files, removing anything that is not in directory 1.
The result is the contents of directory 1 spewed out into to the base
of repository A.  You probably want to import these files into
repository B within a directory, so move them into one now (lines
5/6). Commit your changes and we're ready to merge these files into
the new repository.

git clone <git repository A url>
cd <git repository A directory>
git remote rm origin
git filter-branch &#x2013;subdirectory-filter <directory 1> &#x2013; &#x2013;all
mkdir <directory 1>
mv \* <directory 1>
git add .
git commit

Merge files into new repository:

Make a copy of repository B if you don't have one already.  On line 3, you'll create a remote connection to repository A as a branch in repository B.  Then simply pull from this branch (containing only the directory you want to move) into repository B.  The pull copies both files and history.  Note: You can use a merge instead of a pull, but pull worked better for me. Finally, you probably want to clean up a bit by removing the remote connection to repository A. Commit and you're all set.

git clone <git repository B url>
cd <git repository B directory>
git remote add repo-A-branch <git repository A directory>
git pull repo-A-branch master
git remote rm repo-A-branch


<a id="orgc22065e"></a>

# Fixing a remote that has incorrect commits

Details about git reset can be found [here](https://git-scm.com/2011/07/11/reset.html) and [here](http://christoph.ruegg.name/blog/git-howto-revert-a-commit-already-pushed-to-a-remote-reposit.html)

-   Supposed you added the wrong remote for a paper repository and pushed the following to the repo nrl:actorsim-core/master branch:
    From l14gfe1.aic.nrl.navy.mil:/opt/git/actorsim/actorsim
    (\*) branch            master     -> FETCH<sub>HEAD</sub>
    5fdf3ff..e61c9f0  master     -> nrl/master
    Merge made by the 'recursive' strategy.
    aaai.bst                            | 1192 <del><del><del><del><del><del><del><del><del><del><del><del><del><del><del><del>++</del></del></del></del></del></del></del></del></del></del></del></del></del></del></del></del>
    aaai17.sty                          |  293 <del><del><del><del>+</del></del></del></del>
    biblio.bib                          | 1220 <del><del><del><del><del><del><del><del><del><del><del><del><del><del><del><del><del>+</del></del></del></del></del></del></del></del></del></del></del></del></del></del></del></del></del>
    fixbib.sty                          |  138 <del>++</del>
    grmacros.tex                        |  225 <del><del><del>+</del></del></del>
    images/first<sub>person.png</sub>             |  Bin 0 -> 345895 bytes
    images/map.png                      |  Bin 0 -> 28687 bytes
    images/third<sub>person.png</sub>             |  Bin 0 -> 405338 bytes
    integrationExample/domain.pddl.txt  |   90 <del>+</del>
    integrationExample/output.txt       |   82 <del>+</del>
    integrationExample/problem.pddl.txt |  257 <del><del><del>++</del></del></del>
    main.pdf                            |  Bin 0 -> 554378 bytes
    main.tex                            |  515 <del><del><del><del><del><del><del>+</del></del></del></del></del></del></del>
    mak-refs.bib                        |  884 <del><del><del><del><del><del><del><del><del><del><del><del>+</del></del></del></del></del></del></del></del></del></del></del></del>
    todo.org                            |  113 <del>++</del>
    15 files changed, 5009 insertions(+)
    create mode 100644 aaai.bst
    create mode 100644 aaai17.sty
    create mode 100644 biblio.bib
    create mode 100644 fixbib.sty
    create mode 100644 grmacros.tex
    create mode 100644 images/first<sub>person.png</sub>
    create mode 100644 images/map.png
    create mode 100644 images/third<sub>person.png</sub>
    create mode 100644 integrationExample/domain.pddl.txt
    create mode 100644 integrationExample/output.txt
    create mode 100644 integrationExample/problem.pddl.txt
    create mode 100644 main.pdf
    create mode 100644 main.tex
    create mode 100644 mak-refs.bib
    create mode 100644 todo.org
    
    Success (10281 ms @ 11/25/2016 11:36:41 AM)
-   You need to remove these from the remote
-   To fix this, you reset the head back to the original commit that it should have been and force a push to the remote. 
    **DO NOT** do this on a live, active branch or you will piss off your developers!
-   First, you determined the proper SHA-1 id using git log, lets call it [before-foobar]
-   Clone a pristine copy of the repo
-   Reset the local repo to the before-foobar sha-1 id:
    git reset [before-foobar] &#x2013;hard
-   Verify with 'git log' that the new state is correct
-   Force the change with push -f
    git push -f
-   Validate that this worked on an existing (but not yet pulled) repo
-   Delete the broken repository or mark it as bad so you don't inadvertently push from it again and recreate the problem


<a id="org3889197"></a>

# Setting the upstream remote for a branch

-   During a push: git push -u origin my<sub>branch</sub>
-   Using short branch command: git branch -u origin/my<sub>branch</sub>
-   Using branch command: git branch &#x2013;set-upstream-to origin/my<sub>branch</sub>


<a id="org1034c84"></a>

# Removing a directory and its history

-   Simple.  Use bfg: <https://rtyley.github.io/bfg-repo-cleaner/>
    bfg is an alias for java -jar bfg.jar
    Note that this needs to be done on a pristine git repo.
    
    To remove folder:
    git rm <folder-name>
    git commit -am"message&#x2026;"
    bfg &#x2013;delete-folder <folder-name>
    \#expire old refs and do garbage collection
    git reflog expire &#x2013;expire=now &#x2013;all && git gc &#x2013;prune=now &#x2013;aggressive && git gc


<a id="orgdf0a1da"></a>

# Fixing permissions issues after moving directories

find . -type f -exec chmod 644 "{}" \\;


<a id="org161137a"></a>

# Creating a git bundle and zip (useful for sofware release)

Examples: 
git bundle create ../20190926<sub>pub</sub><sub>release</sub>/20190926-actorsim-core-develop<sub>git</sub>-bundle develop
git bundle create ../20190926<sub>pub</sub><sub>release</sub>/20190926-actorsim-core-all-bundle &#x2013;all
zip &#x2013;exclude="**.git**" -r ../actorsim/20190926<sub>pub</sub><sub>release</sub>/actorsim-roborescue-teamNRL-develop.zip team-nrl-git


# Git subtree
https://www.atlassian.com/git/tutorials/git-subtree
https://git-scm.com/docs/githooks
https://git-scm.com/docs/git-init#_template_directory
https://thomasvilhena.com/2021/11/prevent-merge-from-specific-branch-git-hook
https://yunwuxin1.gitbooks.io/git/content/en/166dfa9a3724f8ec184652066005eef6/b106e7f9582f0a0eb1d787c4b8a1d093.html
http://johnatten.com/2013/03/16/git-subtree-merge-the-quick-version/
https://github.com/ande3577/Git-Subtree-Workflow-Proposal/wiki/Subtree-Based-Workflow
https://www.codeproject.com/Articles/562949/ManagingplusNestedplusLibrariesplusUsingplustheplu#Subtree-Merge-Diagram
https://stackoverflow.com/questions/24709704/apply-gradle-file-from-different-repository/24709789#24709789


