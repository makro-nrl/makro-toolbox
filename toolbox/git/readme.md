
# Table of Contents



git-sync.sh
org-git-commit-and-push.sh

-   <span class="timestamp-wrapper"><span class="timestamp">[2015-09-16 Wed] </span></span> Files from Bernt Hansen's orgmode [tutorial](http://doc.norang.ca/org-mode.html#GitSync). org-git-sync checks in **and** commits files on the current      branch, which for me would lead to lots of needless auto-commit messages in the log.  His git-sync script &#x2013; the one I thought was for committing interim changes to a local branch &#x2013; his script actually syncs a group of git repos.  They are interesting to know about, however, so they'll stay in my toolbox for now.

