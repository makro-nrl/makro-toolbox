;;; thunderlink.el - Support for links using thunderlink
     
(require 'org)

(org-add-link-type "thunderlink" 'org-thunderlink-open)

(defun org-thunderlink-open (thunderlink-id)
  "Opens an email given by thunkderlink-id in Thunderbird with the Thunderlink addon."
  (start-process "thunderlink" nil "C:\\Program Files (x86)\\Mozilla Thunderbird\\thunderbird" "-thunderlink" (concat "thunderlink:" thunderlink-id)))

;;"C:\Program Files (x86)\Mozilla Thunderbird\thunderbird" -thunderlink  thunderlink://messageid=CADR62dZO73HFgcTJAGzcgR2iaS2LcSiwtBcNOmgZTwuqL7Cgig@mail.gmail.com
