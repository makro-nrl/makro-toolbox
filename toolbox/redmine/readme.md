# Redmine Ubuntu 20.04 Install

The following are notes for setting up Redmine on an Ubuntu Machine.

I recommend setting up local development install, perhaps in WSL, to test out changes and learn the program.  Once you have that working, set up a deployed server that you will use for your team.  At that point, it might be good to establish a proper database for your local developement that is a clone of your deployed version and write a couple of scripts to backup the deployed server but modify the config so you can easily switch users, test configuration settings, etc.

## Local Dev install
https://redmine.org/projects/redmine/wiki/HowTo_Install_Redmine_50x_on_Ubuntu_2004_with_Apache2

https://redmine.org/projects/redmine/wiki/RedmineInstall

## Full deployed server with MySQL database



### Disable access from anything but localhost

https://unix.stackexchange.com/questions/121456/can-i-configure-apache-to-limit-access-to-a-directory-only-from-my-ssh-connectio
https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04
https://stackoverflow.com/questions/1250055/to-allow-only-localhost-in-apaches-000-default
https://webmasters.stackexchange.com/questions/59624/allowing-access-to-an-apache-virtual-host-from-the-local-network-only


## Update Local Dev install with data from deployed server

### Turn off MySQL auto start (optional)

