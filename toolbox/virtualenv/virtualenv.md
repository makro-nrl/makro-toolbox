Handy Virtualenv Commands
#########################


Create a new virtual environment
---------------------------------

If you have virtualenvwrapper:

mkvirtualenv [-a project_path] [-i package] [-r requirements_file] [virtualenv options] ENVNAME