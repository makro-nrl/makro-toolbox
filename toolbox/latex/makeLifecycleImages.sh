#!/bin/bash


items=(  \
	 # formulate \
	 # select \
	 # expand \
	 # commit \
	 # dispatch \
	 # evaluate \
	 # finish \
	 # monitor \
	 # resolve \
	 # continue \
	 # adjust \
	 # repair \
	 # reexpand \
	 # replan \
	 
	 # formulated \
	 # selected \
	 # expanded \
	 # committed \
	 # dispatched \
	 # evaluated \
	 # finished \

	 # inactive \
	 # waiting \
	 # executing \
	 # completing \
	 # failing \
	 # iterationEnded \
	 # completed \
	 # skipped \

	 # execParentDispatched \
	 execParentFinished \
	 # execExit \
	 # execStart \
	 # execPre \
	 # execInvariant \
	 # execEnd \
	 # execPost \
	 # execCompletion \
	 # execRepeat \
	 # execParentWaiting \

	 # execSuccess \
	 # execSkipped \
	 # execFailed \
	 # execInterrupted 


)

for item in ${items[@]}
    do
	cat template.tex | sed s/"CONTENT"/"$item"/g > "$item.tex"
	pdflatex "$item.tex"
	convert -density 600 -size 400x100 "$item.pdf" -quality 90 "images/lifecycle/$item.png"
	mv "$item.pdf" images/lifecycle
	rm $item.*
    done
