

Docker container cannot access outside
======================================

cannot access network
---------------------
  - check if it is a dns issue
  - Determine what your DNS is: `nmcli dev show | grep 'IP4.DNS'`
  - run docker with the dns setting `docker run --dns <yours> busybox`
  - if it fixes the issue:
    - add your dns to /etc/docker/daemon.json
      ```
      {
        "dns": ["10.0.0.2", "8.8.8.8"]
      }
      ```
    - restart the docker service
      ```
      sudo service docker restart
      ```
    - test the result:
      ```
      docker run busybox nslookup google.com
      ```
ubuntu installation hangs on keyboard setup
-------------------------------------------

- Option 1: disable keyboard interaction by adding the following to your dockerfile:
```
ENV DEBIAN_FRONTEND noninteractive
```

- Option 2: create a file "keyboard" with the following contents (or copy the one in this directory)
  ```
# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT=""
XKBOPTIONS=""

BACKSPACE="guess"
```

- add a command to copy the keyboard file in
```
COPY ./keyboard /etc/default/keyboard
```

