Note from Tiago about his planning [video](https://www.youtube.com/watch?v=IXVVf-yGVcY)
=======================================================================================

I used two main tools for putting that video together: Openshot
(<http://www.openshot.org/>) for video editing and Inkscape
(<https://inkscape.org/en/>) for creating the annotations as images
(with transparent background).

Handbrake is another possible application for video editing
===========================================================

I haven\'t used it yet, since I have been satisfied with openshot.

Resizing using openshot
=======================

You can resize a video by exporting a video. The easiest way to select a
new size is to pick a Web -\> Youtube format and choose an appropriate
quality.


# Showing keystrokes during video

[How to Show Keystrokes on Windows PC Screen - YouTube](https://www.youtube.com/watch?v=08OeYJ8H5nw)
